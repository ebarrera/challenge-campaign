FROM node:carbon

LABEL author="Erick Barrera"

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY . /usr/src/app
RUN npm install
RUN npm install -g sequelize-cli

EXPOSE 3000