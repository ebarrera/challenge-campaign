# Campaign challenge

The next repository contains all code necessary to create an API for mini campaigns service those are applied on products.

## Architecture

I propose the next simple architecture based with one proxy/balancer, n-webservices containers, and one database

![Architecture](https://bitbucket.org/ebarrera/challenge-campaign/raw/d1acfd158b5fca18232602520cb076e62350c6e8/architecure.png)

## Model

I tried to solve the problem with the following model

![Model](https://bitbucket.org/ebarrera/challenge-campaign/raw/b300b8b20ded00a2b578ce2df8d939a89a266152/campaign_challenge_model.png)

## Business rules

* The owners only can update or delete their campaigns
* The owners only can update their password

### Stack used

The project is currently extended with the following packages and libs.

|  packages / libs      | 
| :-------------------: |
|  Express              |
|  Graphql              |
|  Gulp                 |
|  JWT                  |
|  Mocha                |
|  Chai                 |
|  Sequelize            |
|  Docker               |
|  Bitbucket pipelines  |
|  Postgresql           |

### Development

Want to contribute? Great!

The project uses docker and it's configured to agil development.

Make a change in your file and instantanously see your updates!

Open your favorite Terminal and run these commands.

Build and run:

```sh
$ docker-compose build
$ docker-compose up
```

If you prefer the background (I don't recommend, because you cannot see the logs):

```sh
$ docker-compose up --build -d
```

If you decide down the services:

```sh
$ docker-compose down
```

The tests have their container with docker, you can run with:

```sh
$ docker-compose -f docker-compose.test.yml up
```

or down

```sh
$ docker-compose -f docker-compose.test.yml down
```

*Notes*

You can run locally (also tests), but you need to know de port from database, enter this command to look up that port:

```sh
$ docker container ls
```

Changes into config.json the port and the host to *localhost* and now you can enter this command:


```sh
$ npm install
$ npm start
```

If you want to test the functionability please enter to graphql console:

Enter to *localhost/graphql* and execute some mutation like this:

```json
mutation {
  campaignValidator(
    input: {
      products: [
        {
        	id: "31422b42-16c3-44b2-b526-ead0115c8967",
        	code: "PANTS"
      	},
      	{
        	id: "36f27340-3398-40a9-8b52-33d083b74ab1",
        	code: "TSHIRT"
      	},
      	{
        	id: "e6a0f8af-f913-4670-b7ff-94b4edee0dc0",
        	code: "HAT"
        },
        {
        	id: "31422b42-16c3-44b2-b526-ead0115c8967",
        	code: "PANTS"
      	}
        
      ]
      customer: {
        name: "Erick"
        email: "my@email.com"
      }
    }
  ){
    customer {
      name
    }
    products {
      id
      code
      price
    }
    subtotal
    discount
    total
  }
}
```