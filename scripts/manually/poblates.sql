/*
 * To poblate products
 * */
insert into public.products (id, code, "name", price, "createdAt", "updatedAt")
	values('31422b42-16c3-44b2-b526-ead0115c8967', 'PANTS', 'Pants', 5.00, '2018-09-06 23:02:28.851', '2018-09-06 23:02:28.851');

insert into public.products (id, code, "name", price, "createdAt", "updatedAt")
	values('36f27340-3398-40a9-8b52-33d083b74ab1', 'TSHIRT', 'T-shirt', 20.00, '2018-09-06 23:03:16.617', '2018-09-06 23:03:16.617');

insert into public.products (id, code, "name", price, "createdAt", "updatedAt")
	values('e6a0f8af-f913-4670-b7ff-94b4edee0dc0', 'HAT', 'Hat', 7.50, '2018-09-06 23:03:45.942', '2018-09-06 23:03:45.942');


/*
 * To poblate owners
 * */
insert into owners (id, "name", "user", password, enabled, "apiKey", "createdAt", "updatedAt") 
	values ('3322893b-34bb-4e46-bb2a-1483acb7664d', 'Peter Parker', 'peter.parker@marvel.com', '$2a$10$l7Aub0wPWSlXM8yTn6ZRlOW1lTq/ncQCXPdg8Yc6LF2IMbkYFXbPa', true, 'f9173958-9046-4a0f-a480-3f8dfa8440fe', current_timestamp, current_timestamp);

insert into owners (id, "name", "user", password, enabled, "apiKey", "createdAt", "updatedAt") 
	values ('4381cfc6-9a50-4f8b-b605-8316bb1c2f16', 'Peter Quill', 'star.lord@marvel.com', '$2a$10$5dcbecpszOBs/HbJZk80yu.2Yzkzz4A3kElNld/iEplqR/UQ95M/W', true, '3372893b-34bb-4e46-bb2a-1483acb7664d', current_timestamp, current_timestamp);
	
/*
 * To poblate campaigns
 * */
insert into campaigns (id, code, "name", description, "createdAt", "updatedAt", owner)
	values( '4381cfc6-9a50-4f8b-b605-8316bb1c2f16', 'PROMO 2x1', '2 por 1', 'Una promo de 2 x 1', current_timestamp, current_timestamp, '3322893b-34bb-4e46-bb2a-1483acb7664d' );

insert into campaigns (id, code, "name", description, "createdAt", "updatedAt", owner)
	values( '4381cfc6-9a50-4f8b-b605-8316bb1c2ff6', 'PROMO3MAS', '3 o mas', 'Una promo de 3 o mas', current_timestamp, current_timestamp, '4381cfc6-9a50-4f8b-b605-8316bb1c2f16' );
	
insert into campaigns (id, code, "name", description, "createdAt", "updatedAt", owner, enabled)
	values( '1258bdac-1922-4f7a-a576-ccb07b88ec21', 'PROMOTEST', 'Test', 'Test promo', current_timestamp, current_timestamp, '4381cfc6-9a50-4f8b-b605-8316bb1c2f16', false);

/*
 * To poblate promotions
 * */
insert into promotions (id, description, "createdAt", "updatedAt", campaign)
	values( '2e772d46-be3a-40f5-a857-6be919067cc8', 'Promo 2 x 1', current_timestamp, current_timestamp, '4381cfc6-9a50-4f8b-b605-8316bb1c2f16');
	
insert into promotions (id, description, "createdAt", "updatedAt", campaign)
	values( 'aaebd46c-2495-4fa9-9d23-15c512912778', 'Promo 3 o mas', current_timestamp, current_timestamp, '4381cfc6-9a50-4f8b-b605-8316bb1c2ff6');
	
insert into promotions (id, description, "createdAt", "updatedAt", campaign)
	values( 'f2f1111d-285c-40f9-b28d-3441ac3262b4', 'Promo test', current_timestamp, current_timestamp, '1258bdac-1922-4f7a-a576-ccb07b88ec21');

/*
 * To poblate condition types
 * */
insert into condition_types (id, code, description, enabled, "createdAt", "updatedAt")
	values( 'ca8bd0bd-e2e8-41d4-8e66-302c3b9d0bb3', 'ITEM_ON_CART', 'Item on cart', true, current_timestamp, current_timestamp );

insert into condition_types (id, code, description, enabled, "createdAt", "updatedAt")
	values( 'a0d858f4-5c59-4bbd-9b51-5ca5d2cf024e', 'TO_PER_SOMETHING', 'To per something', true, current_timestamp, current_timestamp );

/*
 * To poblate conditions
 * */
insert into conditions (id, comparator, value, product, "logicalConnector", "createdAt", "updatedAt", promotion, "conditionType" )
	values( 'e9b7706e-ca48-4517-a423-da40fe3f0642', 'GREATHER_EQUAL_TO', '3', '36f27340-3398-40a9-8b52-33d083b74ab1', 'AND', current_timestamp, current_timestamp, 'aaebd46c-2495-4fa9-9d23-15c512912778', 'ca8bd0bd-e2e8-41d4-8e66-302c3b9d0bb3');
	
insert into conditions (id, comparator, value, product, "logicalConnector", "createdAt", "updatedAt", promotion, "conditionType" )
	values( 'b23627ee-bdd3-47ea-bc52-777f3fa7e4ed', 'EQUAL_TO', '2,1', '31422b42-16c3-44b2-b526-ead0115c8967', 'AND', current_timestamp, current_timestamp, '2e772d46-be3a-40f5-a857-6be919067cc8', 'a0d858f4-5c59-4bbd-9b51-5ca5d2cf024e');


/*
 * To poblate effect types
 * */
insert into effect_types (id, code, description, enabled, "createdAt", "updatedAt")
	values ('51eb201c-5f2e-46e4-8d1e-9ffe318e8f03', 'ITEM_DISCOUNT_FIXED', 'Item discount fixed', true, current_timestamp, current_timestamp );
	
insert into effect_types (id, code, description, enabled, "createdAt", "updatedAt")
	values ('81210e9d-362a-4b7f-8b33-3f36340578c4', 'ITEM_DISCOUNT_PERCENT', 'Item discount percent', true, current_timestamp, current_timestamp );
	
insert into effect_types (id, code, description, enabled, "createdAt", "updatedAt")
	values ('e9b7706e-ca48-4517-a423-da40fe3f0642', 'CART_DISCOUNT_FIXED', 'Cart discount fixed', true, current_timestamp, current_timestamp );
	
insert into effect_types (id, code, description, enabled, "createdAt", "updatedAt")
	values ('422cf62d-40e1-4dc2-bb7e-a00efbcaa6a8', 'CART_DISCOUNT_PERCENT', 'Cart discount percent', true, current_timestamp, current_timestamp );

insert into effect_types (id, code, description, enabled, "createdAt", "updatedAt")
	values ('aaebd46c-2495-4fa9-9d23-15c512912778', 'TO_PER_SOMETHING', 'To percent something', true, current_timestamp, current_timestamp );
	
/*
 * To poblate effects
 * */
insert into effects (id, value, product, "createdAt", "updatedAt", promotion, "effectType")
	values( 'b23627ee-bdd3-47ea-bc52-777f3fa7e4ed', '19', '36f27340-3398-40a9-8b52-33d083b74ab1', current_timestamp, current_timestamp, 'aaebd46c-2495-4fa9-9d23-15c512912778', '51eb201c-5f2e-46e4-8d1e-9ffe318e8f03');

insert into effects (id, value, product, "createdAt", "updatedAt", promotion, "effectType")
	values( '1258bdac-1922-4f7a-a576-ccb07b88ec21', '2,1', '31422b42-16c3-44b2-b526-ead0115c8967', current_timestamp, current_timestamp, '2e772d46-be3a-40f5-a857-6be919067cc8', 'aaebd46c-2495-4fa9-9d23-15c512912778');
