import * as express from 'express';
import * as graphqlHTTP from 'express-graphql';
import * as expressReqId from 'express-request-id';
import * as cors from 'cors';
import * as compression from 'compression';
import * as helmet from 'helmet';

import db from './models'
import schema from './graphql/schema';
import serviceloaders from './service';
import { getErrorCode } from './utils/errors';
import testSchema from './graphql/resources/otherImplementation/test.schema';

import { DataLoaderFactory } from './graphql/dataloaders/DataLoaderFactory';
import { RequestedFields } from './graphql/ast/RequestedFields';
import { extractJwtMiddleware } from './middlerwares/extract-jwt.middleware';
import { morganMiddleware } from './middlerwares/morgan.middleware';


class App {

    public express: express.Application;
    private dataLoaderFactory: DataLoaderFactory;
    private requestedFields: RequestedFields;

    constructor() {
        this.express = express();
        this.init();
    }

    private init(): void {
        this.requestedFields = new RequestedFields();
        this.dataLoaderFactory = new DataLoaderFactory( db, this.requestedFields );
        this.middleware();
    }

    private middleware(): void {
        const addRequestId = expressReqId();

        this.express.get( '/health', function (req, res) {
            res.status(200).send();
        });

        this.express.use(addRequestId);
        if( process.env.NODE_ENV !== 'test') this.express.use(morganMiddleware);

        this.express.use( cors({
            origin: '*',
            methods: [ 'POST', 'GET' ],
            allowedHeaders: [ 'Content-Type', 'Authorization', 'Accept-Encoding' ],
            preflightContinue: false,
            optionsSuccessStatus: 204
        }));

        this.express.use(compression());
        this.express.use(helmet());

        this.express.use( '/graphql',

            extractJwtMiddleware(),
        
            (req, res, next) => {
                req['context']['db'] = db;
                req['context']['serviceloaders'] = serviceloaders;
                req['context']['dataloaders'] = this.dataLoaderFactory.getLoaders();
                req['context']['requestedFields'] = this.requestedFields;
                next();
            },

            graphqlHTTP((req) => ({
                schema: schema,
                graphiql: process.env.NODE_ENV === 'development',
                context: req['context'],
                formatError: (error) => {
                    return {
                        message: error.message,
                        state: error.originalError && error.originalError.state,
                        path: error.path
                    }
                }
            }))
        );

        /*
        * Test mappgin by resource, this just an example, but could be a bad practice
        * it's necessary look for another option.
        */
        this.express.use( '/owners',

            extractJwtMiddleware(),

            (req, res, next) => {
                req['context']['db'] = db;
                req['context']['dataloaders'] = this.dataLoaderFactory.getLoaders();
                req['context']['requestedFields'] = this.requestedFields;
                next();
            },

            graphqlHTTP((req) => ({
                schema: testSchema,
                graphiql: process.env.NODE_ENV === 'development',
                context: req['context'],
                formatError: (error) => {
                    console.error(error);
                    return {
                        message: error.message,
                        state: error.originalError && error.originalError.state,
                        path: error.path
                    }
                }
            }))
        );
    }
}

export default new App().express;