const campaigns = [
    { 
        id: '4381cfc6-9a50-4f8b-b605-8316bb1c2f16',
        code: 'PROMO 2x1',
        name: '2 por 1',
        description: 'Una promo de 2 x 1',
        enabled: true,
        isAutomatic: true,
        owner: '3322893b-34bb-4e46-bb2a-1483acb7664d'
    },
    { 
        id: '4381cfc6-9a50-4f8b-b605-8316bb1c2ff6',
        code: 'PROMO3MAS',
        name: '3 o mas',
        description: 'Una promo de 3 o mas',
        enabled: true,
        isAutomatic: true,
        owner: '4381cfc6-9a50-4f8b-b605-8316bb1c2f16'
    },
    { 
        id: '1258bdac-1922-4f7a-a576-ccb07b88ec21',
        code: 'PROMOTEST',
        name: 'Test',
        description: 'Test promo',
        enabled: false,
        isAutomatic: true,
        owner: '4381cfc6-9a50-4f8b-b605-8316bb1c2f16'
    }
];

export default campaigns;