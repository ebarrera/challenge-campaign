const conditionTypes = [
    { 
        id: 'ca8bd0bd-e2e8-41d4-8e66-302c3b9d0bb3',
        code: 'ITEM_ON_CART',
        description: 'Item on cart',
        enabled: true
    },
    { 
        id: 'a0d858f4-5c59-4bbd-9b51-5ca5d2cf024e',
        code: 'TO_PER_SOMETHING',
        description: 'To per something',
        enabled: true
    }
];

export default conditionTypes;