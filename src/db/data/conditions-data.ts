const conditions = [
    { 
        id: 'e9b7706e-ca48-4517-a423-da40fe3f0642',
        comparator: 'GREATHER_EQUAL_TO',
        value: '3',
        product: '36f27340-3398-40a9-8b52-33d083b74ab1',
        logicalConnector: 'AND',
        promotion: 'aaebd46c-2495-4fa9-9d23-15c512912778',
        conditionType: 'ca8bd0bd-e2e8-41d4-8e66-302c3b9d0bb3'
    },
    { 
        id: 'b23627ee-bdd3-47ea-bc52-777f3fa7e4ed',
        comparator: 'EQUAL_TO',
        value: '2,1',
        product: '31422b42-16c3-44b2-b526-ead0115c8967',
        logicalConnector: 'AND',
        promotion: '2e772d46-be3a-40f5-a857-6be919067cc8',
        conditionType: 'a0d858f4-5c59-4bbd-9b51-5ca5d2cf024e'
    }
];

export default conditions;