const effectTypes = [
    { 
        id: '51eb201c-5f2e-46e4-8d1e-9ffe318e8f03',
        code: 'ITEM_DISCOUNT_FIXED',
        description: 'Item discount fixed',
        enabled: true
    },
    { 
        id: '81210e9d-362a-4b7f-8b33-3f36340578c4',
        code: 'ITEM_DISCOUNT_PERCENT',
        description: 'Item discount percent',
        enabled: true
    },
    { 
        id: 'e9b7706e-ca48-4517-a423-da40fe3f0642',
        code: 'CART_DISCOUNT_FIXED',
        description: 'Cart discount fixed',
        enabled: true
    },
    { 
        id: '422cf62d-40e1-4dc2-bb7e-a00efbcaa6a8',
        code: 'CART_DISCOUNT_PERCENT',
        description: 'Cart discount percent',
        enabled: true
    },
    { 
        id: 'aaebd46c-2495-4fa9-9d23-15c512912778',
        code: 'TO_PER_SOMETHING',
        description: 'To percent something',
        enabled: true
    }
];

export default effectTypes;