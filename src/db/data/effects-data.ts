const effects = [
    { 
        id: 'b23627ee-bdd3-47ea-bc52-777f3fa7e4ed',
        value: '19',
        product: '36f27340-3398-40a9-8b52-33d083b74ab1',
        promotion: 'aaebd46c-2495-4fa9-9d23-15c512912778',
        effectType: '51eb201c-5f2e-46e4-8d1e-9ffe318e8f03'
    },
    { 
        id: '1258bdac-1922-4f7a-a576-ccb07b88ec21',
        value: '2,1',
        product: '31422b42-16c3-44b2-b526-ead0115c8967',
        promotion: '2e772d46-be3a-40f5-a857-6be919067cc8',
        effectType: 'aaebd46c-2495-4fa9-9d23-15c512912778'
    }
];

export default effects;