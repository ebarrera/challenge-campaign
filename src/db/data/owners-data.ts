const owners = [
    { 
        id: '3322893b-34bb-4e46-bb2a-1483acb7664d',
        name: 'Peter Parker',
        user: 'peter.parker@marvel.com',
        password: '$2a$10$l7Aub0wPWSlXM8yTn6ZRlOW1lTq/ncQCXPdg8Yc6LF2IMbkYFXbPa',
        enabled: true,
        apiKey: 'f9173958-9046-4a0f-a480-3f8dfa8440fe'
    },
    { 
        id: '4381cfc6-9a50-4f8b-b605-8316bb1c2f16',
        name: 'Peter Quill',
        user: 'star.lord@marvel.com',
        password: '$2a$10$5dcbecpszOBs/HbJZk80yu.2Yzkzz4A3kElNld/iEplqR/UQ95M/W',
        enabled: true,
        apiKey: '3372893b-34bb-4e46-bb2a-1483acb7664d'
    }
];

export default owners;