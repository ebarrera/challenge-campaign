const products = [
    { 
        id: '31422b42-16c3-44b2-b526-ead0115c8967',
        code: 'PANTS',
        name: 'Pants',
        price: 5.00
    },
    { 
        id: '36f27340-3398-40a9-8b52-33d083b74ab1',
        code: 'TSHIRT',
        name: 'T-shirt',
        price: 20.00
    },
    { 
        id: 'e6a0f8af-f913-4670-b7ff-94b4edee0dc0',
        code: 'hat',
        name: 'Hat',
        price: 7.50
    }
];

export default products;