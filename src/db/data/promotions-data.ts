const promotions = [
    { 
        id: '2e772d46-be3a-40f5-a857-6be919067cc8',
        description: 'Promo 2 x 1',
        campaign: '4381cfc6-9a50-4f8b-b605-8316bb1c2f16'
    },
    { 
        id: 'aaebd46c-2495-4fa9-9d23-15c512912778',
        description: 'Promo 3 o mas',
        campaign: '4381cfc6-9a50-4f8b-b605-8316bb1c2ff6'
    },
    { 
        id: 'f2f1111d-285c-40f9-b28d-3441ac3262b4',
        description: 'Promo test',
        campaign: '1258bdac-1922-4f7a-a576-ccb07b88ec21'
    },
];

export default promotions;