import * as Sequelize from 'sequelize';
import { QueryInterface } from 'sequelize';
import { ConditionTypes } from '../../models/ConditionTypeModel';

module.exports = {
    up: (queryInterface: QueryInterface) => {
        return queryInterface.createTable( "condition_types", {
            id: {
                type: Sequelize.UUID,
                allowNull: false,
                primaryKey: true,
                defaultValue: Sequelize.UUIDV4
            },
            code: {
                type: Sequelize.ENUM,
                values: Object.keys( ConditionTypes ),
                allowNull: false,
                unique: true
            },
            description: {
                type: Sequelize.STRING,
                allowNull: true
            },
            enabled: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: true
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface: QueryInterface) => {
        return queryInterface.dropTable("condition_types");
    }
};