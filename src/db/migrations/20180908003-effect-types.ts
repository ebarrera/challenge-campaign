import * as Sequelize from 'sequelize';
import { QueryInterface } from 'sequelize';
import { EffectTypes } from '../../models/EffectTypeModel';

module.exports = {
    up: (queryInterface: QueryInterface) => {
        return queryInterface.createTable( "effect_types", {
            id: {
                type: Sequelize.UUID,
                allowNull: false,
                primaryKey: true,
                defaultValue: Sequelize.UUIDV4
            },
            code: {
                type: Sequelize.ENUM,
                values: Object.keys( EffectTypes ),
                allowNull: false,
                unique: true
            },
            description: {
                type: Sequelize.STRING,
                allowNull: true
            },
            enabled: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: true
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface: QueryInterface) => {
        return queryInterface.dropTable("effect_types");
    }
};