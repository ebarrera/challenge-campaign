import * as Sequelize from "sequelize";
import { QueryInterface } from "sequelize";

module.exports = {
    up: (queryInterface: QueryInterface) => {
        return queryInterface.createTable( "promotions", {
            id: {
                type: Sequelize.UUID,
                allowNull: false,
                primaryKey: true,
                defaultValue: Sequelize.UUIDV4
            },
            description: {
                type: Sequelize.STRING,
                allowNull: false
            },
            banner: {
                type: Sequelize.TEXT,
                allowNull: true
            },
            campaign: {
                type: Sequelize.UUID,
                allowNull: false,
                onUpdate: 'CASCADE',
                references: {
                    model: 'campaigns',
                    key: 'id'
                }
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface: QueryInterface) => {
        return queryInterface.dropTable("promotions");
    },
};