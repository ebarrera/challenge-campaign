import * as Sequelize from "sequelize";
import { QueryInterface } from "sequelize";
import { Comparator } from '../../models/enums/ComparatorEnum';
import { LogicalConnector } from '../../models/enums/LogicalConnectorEnum';

module.exports = {
    up: (queryInterface: QueryInterface) => {
        return queryInterface.createTable( "conditions", {
            id: {
                type: Sequelize.UUID,
                allowNull: false,
                primaryKey: true,
                defaultValue: Sequelize.UUIDV4
            },
            comparator: {
                type: Sequelize.ENUM,
                values: Comparator.getAll(),
                allowNull: false
            },
            value: {
                type: Sequelize.STRING,
                allowNull: true
            },
            product: {
                type: Sequelize.UUID,
                allowNull: true
            },
            logicalConnector: {
                type: Sequelize.ENUM,
                values: LogicalConnector.getAll(),
                allowNull: false
            },
            promotion: {
                type: Sequelize.UUID,
                allowNull: false,
                onUpdate: 'CASCADE',
                references: {
                    model: 'promotions',
                    key: 'id'
                }
            },
            conditionType: {
                type: Sequelize.UUID,
                allowNull: false,
                onUpdate: 'CASCADE',
                references: {
                    model: 'condition_types',
                    key: 'id'
                }
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface: QueryInterface) => {
        return queryInterface.dropTable("conditions");
    },
};