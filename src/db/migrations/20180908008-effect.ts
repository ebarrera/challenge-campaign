import * as Sequelize from "sequelize";
import { QueryInterface } from "sequelize";
import { Comparator } from '../../models/enums/ComparatorEnum';
import { LogicalConnector } from '../../models/enums/LogicalConnectorEnum';

module.exports = {
    up: (queryInterface: QueryInterface) => {
        return queryInterface.createTable( "effects", {
            id: {
                type: Sequelize.UUID,
                allowNull: false,
                primaryKey: true,
                defaultValue: Sequelize.UUIDV4
            },
            value: {
                type: Sequelize.STRING,
                allowNull: true
            },
            max: {
                type: Sequelize.INTEGER,
                allowNull: false,
                defaultValue: -1
            },
            product: {
                type: Sequelize.UUID,
                allowNull: true
            },
            promotion: {
                type: Sequelize.UUID,
                allowNull: false,
                onUpdate: 'CASCADE',
                references: {
                    model: 'promotions',
                    key: 'id'
                }
            },
            effectType: {
                type: Sequelize.UUID,
                allowNull: false,
                onUpdate: 'CASCADE',
                references: {
                    model: 'effect_types',
                    key: 'id'
                }
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface: QueryInterface) => {
        return queryInterface.dropTable("effects");
    },
};