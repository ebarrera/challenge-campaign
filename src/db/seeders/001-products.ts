import db from '../../models';
import products from '../data/products-data';

module.exports = {
    up: () => {
        return db.Product.bulkCreate( products );
    },
    down: () => {
        return db.Product.destroy({ where: {} });
    },
};