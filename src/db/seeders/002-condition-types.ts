import db from '../../models';
import conditionTypes from '../data/condition-types-data';

module.exports = {
    up: () => {
        return db.ConditionType.bulkCreate( conditionTypes );
    },
    down: () => {
        return db.ConditionType.destroy({ where: {} });
    },
};