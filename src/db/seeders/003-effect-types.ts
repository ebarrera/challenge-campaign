import db from '../../models';
import effectTypes from '../data/effect-types-data';

module.exports = {
    up: () => {
        return db.EffectType.bulkCreate( effectTypes );
    },
    down: () => {
        return db.EffectType.destroy({ where: {} });
    },
};