import db from '../../models';
import owners from '../data/owners-data';

module.exports = {
    up: () => {
        return db.Owner.bulkCreate( owners );
    },
    down: () => {
        return db.Owner.destroy({ where: {} });
    },
};