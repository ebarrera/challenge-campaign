import db from '../../models';
import campaigns from '../data/campaigns-data';

module.exports = {
    up: () => {
        return db.Campaign.bulkCreate( campaigns );
    },
    down: () => {
        return db.Campaign.destroy({ where: {} });
    },
};