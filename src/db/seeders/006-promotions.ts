import db from '../../models';
import promotions from '../data/promotions-data';

module.exports = {
    up: () => {
        return db.Promotion.bulkCreate( promotions );
    },
    down: () => {
        return db.Promotion.destroy({ where: {} });
    },
};