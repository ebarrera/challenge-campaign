import db from '../../models';
import conditions from '../data/conditions-data';

module.exports = {
    up: () => {
        return db.Condition.bulkCreate( conditions );
    },
    down: () => {
        return db.Condition.destroy({ where: {} });
    },
};