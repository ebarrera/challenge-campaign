import db from '../../models';
import effects from '../data/effects-data';

module.exports = {
    up: () => {
        return db.Effect.bulkCreate( effects );
    },
    down: () => {
        return db.Effect.destroy({ where: {} });
    },
};