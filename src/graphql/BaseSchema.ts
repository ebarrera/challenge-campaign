import { GraphQLSchema } from 'graphql';

export abstract class BaseSchema {

    private SchemaDefinition : string = `

        type Schema {
            query: Query
            mutation: Mutation
        }
        
    `;

    getDefinition(): string {
        return this.SchemaDefinition;
    }

    abstract schema(): GraphQLSchema;
}

export interface BaseDefinition {
    type(): string;
}