import { DataLoaderParameter } from '../../interfaces/DataLoaderParameterInterface';
import { RequestedFields } from '../ast/RequestedFields';
import { CampaignModel, CampaignInstance } from '../../models/CampaignModel';

export class CampaignLoader {

    static batchCampaigns(Campaign: CampaignModel, params: DataLoaderParameter<string>[], requestedFields: RequestedFields): Promise<CampaignInstance[]> {

        let ids: string[] = params.map(param => param.key);
        
        return Promise.resolve(
                Campaign.findAll({
                    where: {
                        id: { $in: ids },
                    },
                    attributes: requestedFields.getFields( params[0].info, {keep: ['id'], exclude: ['owner']} )
                })
            );
    }

}