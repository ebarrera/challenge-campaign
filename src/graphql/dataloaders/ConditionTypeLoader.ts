import { DataLoaderParameter } from '../../interfaces/DataLoaderParameterInterface';
import { RequestedFields } from '../ast/RequestedFields';
import { ConditionTypeModel, ConditionTypeInstance } from '../../models/ConditionTypeModel';

export class ConditionTypeLoader {

    static batchConditionTypes(ConditionType: ConditionTypeModel, params: DataLoaderParameter<string>[], requestedFields: RequestedFields): Promise<ConditionTypeInstance[]> {

        let ids: string[] = params.map(param => param.key);
        
        return Promise.resolve(
                ConditionType.findAll({
                    where: {
                        id: { $in: ids },
                    },
                    attributes: requestedFields.getFields( params[0].info, {keep: ['id']} )
                })
            );
    }

}