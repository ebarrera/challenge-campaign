import * as DataLoader from 'dataloader';

import { DbConnection } from '../../interfaces/DbConnectionInterface';
import { DataLoaders } from '../../interfaces/DataLoadersInterface';
import { RequestedFields } from '../ast/RequestedFields';
import { DataLoaderParameter } from '../../interfaces/DataLoaderParameterInterface';

import { CampaignInstance } from '../../models/CampaignModel';
import { CampaignLoader } from './CampaignLoader';
import { OwnerInstance } from '../../models/OwnerModel';
import { OwnerLoader } from './OwnerLoader';
import { PromotionInstance } from '../../models/PromotionModel';
import { PromotionLoader } from './PromotionLoader';
import { ConditionTypeInstance } from '../../models/ConditionTypeModel';
import { ConditionTypeLoader } from './ConditionTypeLoader';
import { EffectTypeInstance } from '../../models/EffectTypeModel';
import { EffectTypeLoader } from './EffectTypeLoader';

export class DataLoaderFactory {

    constructor(
        private db: DbConnection,
        private requestedFields: RequestedFields
    ) {}

    getLoaders(): DataLoaders {
        return {
            campaignLoader: new DataLoader<DataLoaderParameter<string>, CampaignInstance>(
                (params: DataLoaderParameter<string>[]) => CampaignLoader.batchCampaigns( this.db.Campaign, params, this.requestedFields ),
                { cacheKeyFn: (param: DataLoaderParameter<string[]>) => param.key }
            ),
            ownerLoader: new DataLoader<DataLoaderParameter<string>, OwnerInstance>(
                (params: DataLoaderParameter<string>[]) => OwnerLoader.batchOwners( this.db.Owner, params, this.requestedFields ),
                { cacheKeyFn: (param: DataLoaderParameter<string[]>) => param.key }
            ),
            promotionLoader: new DataLoader<DataLoaderParameter<string>, PromotionInstance>(
                (params: DataLoaderParameter<string>[]) => PromotionLoader.batchPromotions( this.db.Promotion, params, this.requestedFields ),
                { cacheKeyFn: (param: DataLoaderParameter<string[]>) => param.key }
            ),
            conditionTypeLoader: new DataLoader<DataLoaderParameter<string>, ConditionTypeInstance>(
                (params: DataLoaderParameter<string>[]) => ConditionTypeLoader.batchConditionTypes( this.db.ConditionType, params, this.requestedFields ),
                { cacheKeyFn: (param: DataLoaderParameter<string[]>) => param.key }
            ),
            effectTypeLoader: new DataLoader<DataLoaderParameter<string>, EffectTypeInstance>(
                (params: DataLoaderParameter<string>[]) => EffectTypeLoader.batchEffectTypes( this.db.EffectType, params, this.requestedFields ),
                { cacheKeyFn: (param: DataLoaderParameter<string[]>) => param.key }
            )
        };
    }
}