import { DataLoaderParameter } from '../../interfaces/DataLoaderParameterInterface';
import { RequestedFields } from '../ast/RequestedFields';
import { EffectTypeModel, EffectTypeInstance } from '../../models/EffectTypeModel';

export class EffectTypeLoader {

    static batchEffectTypes(EffectType: EffectTypeModel, params: DataLoaderParameter<string>[], requestedFields: RequestedFields): Promise<EffectTypeInstance[]> {

        let ids: string[] = params.map(param => param.key);
        
        return Promise.resolve(
                EffectType.findAll({
                    where: {
                        id: { $in: ids },
                    },
                    attributes: requestedFields.getFields( params[0].info, {keep: ['id']} )
                })
            );
    }

}