import { DataLoaderParameter } from '../../interfaces/DataLoaderParameterInterface';
import { RequestedFields } from '../ast/RequestedFields';
import { OwnerModel, OwnerInstance } from '../../models/OwnerModel';

export class OwnerLoader {

    static batchOwners(Owner: OwnerModel, params: DataLoaderParameter<string>[], requestedFields: RequestedFields): Promise<OwnerInstance[]> {

        let ids: string[] = params.map(param => param.key);
        
        return Promise.resolve(
                Owner.findAll({
                    where: {
                        id: { $in: ids },
                    },
                    attributes: requestedFields.getFields( params[0].info, {keep: ['id']} )
                })
            );
    }

}