import { DataLoaderParameter } from '../../interfaces/DataLoaderParameterInterface';
import { RequestedFields } from '../ast/RequestedFields';
import { PromotionModel, PromotionInstance } from '../../models/PromotionModel';

export class PromotionLoader {

    static batchPromotions(Promotion: PromotionModel, params: DataLoaderParameter<string>[], requestedFields: RequestedFields): Promise<PromotionInstance[]> {

        let ids: string[] = params.map(param => param.key);
        
        return Promise.resolve(
                Promotion.findAll({
                    where: {
                        id: { $in: ids },
                    },
                    attributes: requestedFields.getFields( params[0].info, {keep: ['id']} )
                })
            );
    }

}