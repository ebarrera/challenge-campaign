import { campaignMutations } from './resources/campaign/campaign.schema';
import { ownerMutations } from './resources/owner/owner.schema';
import { promotionMutations } from './resources/promotion/promotion.schema';
import { conditionTypeMutations } from './resources/conditionType/conditionType.schema';
import { conditionMutations } from './resources/condition/condition.schema';
import { effectTypeMutations } from './resources/effectType/effectType.schema';
import { effectMutations } from './resources/effect/effect.schema';
import { tokenMutations } from './resources/token/token.schema';
import { orderMutations } from './resources/order/order.schema';
import { productMutations } from './resources/product/product.schema';

const Mutation = `
    type Mutation {
        ${tokenMutations}
        ${campaignMutations}
        ${ownerMutations}
        ${promotionMutations}
        ${conditionTypeMutations}
        ${conditionMutations}
        ${effectTypeMutations}
        ${effectMutations}
        ${productMutations}
        ${orderMutations}
    }
`;

export {
    Mutation
}