import { campaignQueries } from './resources/campaign/campaign.schema';
import { ownerQueries } from './resources/owner/owner.schema';
import { promotionQueries } from './resources/promotion/promotion.schema';
import { conditionQueries } from './resources/condition/condition.schema';
import { conditionTypeQueries } from './resources/conditionType/conditionType.schema';
import { effectTypeQueries } from './resources/effectType/effectType.schema';
import { effectQueries } from './resources/effect/effect.schema';
import { productQueries } from './resources/product/product.schema';

const Query = `
    type Query {
        ${campaignQueries}
        ${ownerQueries}
        ${promotionQueries}
        ${conditionTypeQueries}
        ${conditionQueries}
        ${effectTypeQueries}
        ${effectQueries}
        ${productQueries}
    }
`;

export {
    Query
}