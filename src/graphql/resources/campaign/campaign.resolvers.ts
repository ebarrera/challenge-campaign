import { Transaction } from 'sequelize';
import { GraphQLResolveInfo } from 'graphql';

import { ResolverContext } from '../../../interfaces/ResolverContextInterface';
import { handleError, throwError } from '../../../utils/utils';
import { CampaignInstance } from '../../../models/CampaignModel';
import { compose } from '../../composable/composable.resolver';
import { authResolvers } from '../../composable/auth.resolver';

export const campaignResolvers = {

    Campaign: {
        owner: (campaign, args, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.dataloaders.ownerLoader
                        .load({
                            key: campaign.get('owner'),
                            info: info
                        }).catch( handleError );
        }
    },
    Query: {
        campaigns: compose( ...authResolvers )((parent, { first = 10, offset = 0}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.Campaign
                        .findAll({
                            limit: first,
                            offset: offset,
                            where: {
                                owner: context.authOwner.id
                            },
                            attributes: context.requestedFields.getFields(info)
                        }).catch( handleError );
        }),
        campaign: (parent, {id}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.Campaign
                        .findById( id, {
                            attributes: context.requestedFields.getFields(info)
                        }).catch( handleError );
        }
    },
    Mutation: {
        createCampaign: compose( ...authResolvers )((parent, {input}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.sequelize.transaction( (t: Transaction) => {
                input.owner = context.authOwner.id;
                return context.db.Campaign.create( input, {transaction: t} );
            }).catch(  handleError );
        }),
        disableCampaign: compose( ...authResolvers )((parent, {id}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.sequelize.transaction( (t: Transaction) => {
                return context.db.Campaign.findById( id )
                        .then( (campagin: CampaignInstance) => {
                            throwError(!campagin,  `Campaign with id ${id} not found` );
                            throwError( campagin.get('owner') !== context.authOwner.id, `Unauthorized! You can only disable your campaigns!`);
                            return campagin.update({enabled: false}, {
                                where: {
                                    id: id
                                },
                                transaction: t
                            })
                        })
            }).catch( handleError );
        }),
        deleteCampaign: compose( ...authResolvers )((parent, {id}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.sequelize.transaction( (t: Transaction) => {
                return context.db.Campaign.findById( id )
                        .then( (campaign: CampaignInstance) => {
                            throwError(!campaign,  `Campaign with id ${id} not found` );
                            throwError( campaign.get('owner') !== context.authOwner.id, `Unauthorized! You can only delete your campaigns!`);
                            return campaign.destroy( {transaction: t} )
                                    .then( post => !!post );
                        })
            }).catch( handleError );
        })
    }

}