const campaignTypes = `

    # Campaign definition type
    type Campaign {
        id: ID!
        code: String!
        name: String!
        description: String
        isAutomatic: Boolean!
        enabled: Boolean!
        createdAt: String!
        owner: Owner!
    }

    input CampaignInput {
        code: String!
        name: String!
        description: String
    }

`;

const campaignQueries = `
    campaigns(first: Int, offset: Int): [Campaign!]!
    campaign(id: ID!): Campaign
`;

const campaignMutations = `
    createCampaign(input: CampaignInput): Campaign
    disableCampaign(id: ID!): Campaign
    deleteCampaign(id: ID!): Boolean
`;

export {
    campaignTypes,
    campaignQueries,
    campaignMutations
}