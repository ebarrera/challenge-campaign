import { Transaction } from 'sequelize';
import { GraphQLResolveInfo } from 'graphql';

import { ResolverContext } from '../../../interfaces/ResolverContextInterface';
import { handleError, throwError } from '../../../utils/utils';
import { ConditionInstance } from '../../../models/ConditionModel';

export const conditionResolvers = {

    Condition: {
        promotion: (condition, args, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.dataloaders.promotionLoader
                    .load({
                        key: condition.get('promotion'),
                        info: info
                    }).catch( handleError );
        },
        conditionType: (condition, args, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.dataloaders.conditionTypeLoader
                    .load({
                        key: condition.get('conditionType'),
                        info: info
                    }).catch( handleError );
        }
    },
    Query: {
        conditions: (parent, {first = 10, offset= 0}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.Condition.findAll({
                limit: first,
                offset: offset,
                attributes: context.requestedFields.getFields(info)
            }).catch( handleError );
        },
        condition: (parent, {id}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.Condition.findById( id, {
                attributes: context.requestedFields.getFields( info )
            }).catch( handleError );
        }
    },
    Mutation: {
        createCondition: (parent, {input}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.sequelize.transaction( (t: Transaction) => {
                return context.db.Condition.create( input, {transaction: t} );
            }).catch(handleError);
        },
        updateCondition: (parent, {id, input}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.sequelize.transaction( (t: Transaction) => {
                return context.db.Condition.findById( id )
                            .then( (condition: ConditionInstance) => {
                                throwError( !condition, `Condition with id ${id} not found` );
                                return condition.update( input, {transaction: t});
                            })
            }).catch( handleError );
        },
        deleteCondition: (parent, {id}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.sequelize.transaction( (t: Transaction) => {
                return context.db.Condition.findById( id )
                            .then( (condition: ConditionInstance) => {
                                throwError( !condition, `Condition with id ${id} not found` );
                                return condition.destroy({transaction: t})
                                            .then( condition => !!condition );
                            })
            }).catch( handleError );
        }
    }

}