import { ConditionTypes } from '../../../models/ConditionTypeModel';
import { Comparator } from '../../../models/enums/ComparatorEnum';
import { LogicalConnector } from '../../../models/enums/LogicalConnectorEnum';

const comparatorString = Comparator.getAll().join( ' ' );
const connectorString = LogicalConnector.getAll().join( ' ' );

const conditionTypes = `

    # Condition definition type
    type Condition {
        id: ID!
        comparator: Comparator!
        value: String
        logicalConnector: LogicalConnector!
        product: String
        promotion: Promotion!
        conditionType: ConditionType!
    }

    enum Comparator {
        ${comparatorString}
    }

    enum LogicalConnector {
        ${connectorString}
    }

    input ConditionInput {
        comparator: Comparator!
        value: String
        logicalConnector: LogicalConnector!
        product: String
        promotion: String!
        conditionType: String!
    }

    input ConditionUpdateInput {
        comparator: Comparator!
        value: String
        logicalConnector: LogicalConnector!
        product: String
    }

`;

const conditionQueries = `
    conditions(first: Int, offset: Int): [Condition!]!
    condition(id: ID!): Condition
`;

const conditionMutations = `
    createCondition(input: ConditionInput!): Condition
    updateCondition(id: ID!, input:ConditionUpdateInput!): Condition
    deleteCondition(id: ID!): Boolean
`;

export {
    conditionTypes,
    conditionQueries,
    conditionMutations
}