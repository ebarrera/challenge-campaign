import { GraphQLResolveInfo } from 'graphql';
import { Transaction } from 'sequelize';

import { ResolverContext } from '../../../interfaces/ResolverContextInterface';
import { handleError, throwError } from '../../../utils/utils';
import { ConditionTypeInstance } from '../../../models/ConditionTypeModel';

export const conditionTypeResolvers = {
    Query: {
        conditionTypes: (parent, {first = 10, offset = 0}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.ConditionType.findAll({
                limit: first,
                offset: offset,
                attributes: context.requestedFields.getFields( info )
            }).catch( handleError );
        },
        conditionType: (parent, {id}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.ConditionType.findById( id, {
                attributes: context.requestedFields.getFields( info )
            }).catch( handleError );
        }
    },
    Mutation: {
        createConditionType: (parent, {input}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.sequelize.transaction( (t: Transaction) => {
                return context.db.ConditionType.create( input, {transaction: t});
            }).catch( handleError );
        },
        disableConditionType: (parent, {id}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.sequelize.transaction( (t: Transaction) => {
                return context.db.ConditionType.findById( id )
                            .then( (conditionType: ConditionTypeInstance) => {
                                throwError( !conditionType, `Condition with id ${id} not found` );
                                return conditionType.update( {enabled: false}, {
                                    where: {
                                        id: id
                                    },
                                    transaction: t
                                })
                            });
            }).catch( handleError );
        }
    }
}