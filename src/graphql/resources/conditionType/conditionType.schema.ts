import { ConditionTypes } from '../../../models/ConditionTypeModel';

const conditionTypeEnumString = Object.keys(ConditionTypes).join( ' ' );

const conditionTypeTypes = `

    # ConditionType definition type
    type ConditionType {
        id: ID!
        code: ConditionTypeEnum!
        description: String
        enabled: Boolean!
    }

    enum ConditionTypeEnum {
        ${conditionTypeEnumString}
    }

    input ConditionTypeInput {
        code: ConditionTypeEnum!
        description: String
        enabled: Boolean
    }

`;

const conditionTypeQueries = `
    conditionTypes(first: Int, offset: Int): [ConditionType!]!
    conditionType(id: ID!): ConditionType
`;

const conditionTypeMutations = `
    createConditionType(input: ConditionTypeInput!): ConditionType
    disableConditionType(id: ID!): ConditionType
`;

export {
    conditionTypeTypes,
    conditionTypeQueries,
    conditionTypeMutations
}