const customerTypes = `

    # Customer definition type
    type Customer {
        name: String!
        email: String!
    }

    input CustomerInput {
        name: String!
        email: String!
    }

`;

export {
    customerTypes
}