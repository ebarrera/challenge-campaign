import { Transaction } from 'sequelize';
import { GraphQLResolveInfo } from 'graphql';

import { ResolverContext } from '../../../interfaces/ResolverContextInterface';
import { handleError, throwError } from '../../../utils/utils';
import { PromotionInstance } from '../../../models/PromotionModel';
import { EffectTypeInstance } from '../../../models/EffectTypeModel';
import { EffectInstance } from '../../../models/EffectModel';

export const effectResolvers = {
  
    Effect: {
        promotion: (effect, args, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.dataloaders.promotionLoader
                    .load({
                        key: effect.get('promotion'),
                        info: info
                    }).catch( handleError );
        },
        effectType: (effect, args, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.dataloaders.effectTypeLoader
                    .load({
                        key: effect.get('effectType'),
                        info: info
                    }).catch( handleError );
        }
    },
    Query: {
        effects: (parent, {first = 10, offset = 0}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.Effect.findAll({
                limit: first,
                offset: offset,
                attributes: context.requestedFields.getFields(info)
            }).catch( handleError );
        },
        effect: (parent, {id}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.Effect.findById( id, {
                attributes: context.requestedFields.getFields(info)
            }).catch( handleError );
        }
    },
    Mutation: {
        createEffect: (parent, {input}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.sequelize.transaction( (t: Transaction) => {
                return context.db.Effect.create( input, {transaction: t});
            }).catch( handleError );
        },
        updateEffect: (parent, {id, input}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.sequelize.transaction( (t: Transaction) => {
                return context.db.Effect.findById( id )
                            .then( (effect: EffectInstance) => {
                                throwError( !effect, `Effect with id ${id} not found` );
                                return effect.update( input, {transaction: t});
                            })
            }).catch( handleError );
        },
        deleteEffect: (parent, {id, input}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.sequelize.transaction( (t: Transaction) => {
                return context.db.Effect.findById( id )
                            .then( (effect: EffectInstance) => {
                                throwError( !effect, `Effect with id ${id} not found` );
                                return effect.destroy({transaction: t})
                                            .then( effect => !!effect );
                            })
            }).catch( handleError );
        }
    }

};