const effectTypes = `

    # Effect definition type
    type Effect {
        id: ID!
        value: String
        max: Int
        product: String
        promotion: Promotion!
        effectType: EffectType!
    }

    input EffectInput {
        value: String
        max: Int
        product: String
        promotion: String!
        effectType: String!
    }

    input EffectUpdateInput {
        value: String
        max: Int
        product: String
        effectType: String
    }

`;

const effectQueries = `
    effects(first: Int, offset: Int): [Effect!]!
    effect(id: ID!): Effect
`;

const effectMutations = `
    createEffect(input: EffectInput!): Effect
    updateEffect(id: ID!, input: EffectUpdateInput!): Effect
    deleteEffect(id: ID!): Boolean
`;

export {
    effectTypes,
    effectQueries,
    effectMutations
}