import { GraphQLResolveInfo } from 'graphql';
import { Transaction } from 'sequelize';

import { ResolverContext } from '../../../interfaces/ResolverContextInterface';
import { handleError, throwError } from '../../../utils/utils';
import { EffectTypeInstance } from '../../../models/EffectTypeModel';

export const effectTypeResolvers = {
    Query: {
        effectTypes: (parent, {first = 10, offset = 0}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.EffectType.findAll({
                limit: first,
                offset: offset,
                attributes: context.requestedFields.getFields( info )
            }).catch( handleError );
        },
        effectType: (parent, {id}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.EffectType.findById( id, {
                attributes: context.requestedFields.getFields( info )
            }).catch( handleError );
        }
    },
    Mutation: {
        createEffectType: (parent, {input}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.sequelize.transaction( (t: Transaction) => {
                return context.db.EffectType.create( input, {transaction: t});
            }).catch( handleError );
        },
        disableEffectType: (parent, {id}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.sequelize.transaction( (t: Transaction) => {
                return context.db.EffectType.findById( id )
                            .then( (effectType: EffectTypeInstance) => {
                                return effectType.update( {enabled: false}, {
                                    where: {
                                        id: id
                                    },
                                    transaction: t
                                })
                            });
            }).catch( handleError );
        }
    }
}