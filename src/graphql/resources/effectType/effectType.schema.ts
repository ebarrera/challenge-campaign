import { EffectTypes } from '../../../models/EffectTypeModel';

const effectTypeEnumString = Object.keys(EffectTypes).join( ' ' );

const effectTypeTypes = `

    # EffectType definition type
    type EffectType {
        id: ID!
        code: EffectTypeEnum!
        description: String
        enabled: Boolean!
    }

    enum EffectTypeEnum {
        ${effectTypeEnumString}
    }

    input EffectTypeInput {
        code: EffectTypeEnum!
        description: String
        enabled: Boolean
    }

`;

const effectTypeQueries = `
    effectTypes(first: Int, offset: Int): [EffectType!]!
    effectType(id: ID!): EffectType
`;

const effectTypeMutations = `
    createEffectType(input: EffectTypeInput!): EffectType
    disableEffectType(id: ID!): EffectType
`;

export {
    effectTypeTypes,
    effectTypeQueries,
    effectTypeMutations
}