import { GraphQLResolveInfo } from 'graphql';

import { ResolverContext } from '../../../interfaces/ResolverContextInterface';
import { OrderAttributes } from '../../../interfaces/CheckoutInterface';
import { throwError } from '../../../utils/utils';

let orderStructure = {
    customer: {
        name: '',
        email: ''
    },
    products: [],
    subtotal: 0,
    total: 0,
    discount: 0
}

export const orderResolvers = {

    Order: {
        customer: (parent, args, context: ResolverContext, info: GraphQLResolveInfo) => {
            const {customer} = orderStructure;
            return Promise.resolve( customer );
        }
    },
    Mutation: {
        campaignValidator: async (parent, {input}, context: ResolverContext, info: GraphQLResolveInfo) => {
            orderStructure = { ...orderStructure, ...input };
            const order: OrderAttributes = orderStructure;
            const status = await context.serviceloaders.validateService.validateProducts( order );
            if( status ) throwError( status.exist, status.message );
            const finalOrder = await context.serviceloaders.validateService.validate( order );            
            return Promise.resolve(finalOrder);
        }
    }

}