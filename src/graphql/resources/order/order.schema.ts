const orderTypes = `

    # Order definition type
    type Order {
        products: [Product!]!
        customer: Customer!
        subtotal: Float!
        discount: Float!
        total: Float!
    }

    input OrderInput {
        products: [ProductRequestInput!]!
        customer: CustomerInput!
        subtotal: Float
        total: Float
        discount: Float
    }

`;

const orderMutations = `
    campaignValidator(input: OrderInput!): Order
`;

export {
    orderTypes,
    orderMutations
}