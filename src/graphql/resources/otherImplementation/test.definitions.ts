import { BaseDefinition } from '../../BaseSchema';

export class TestDefinition implements BaseDefinition {
    
    private definitions = `

        # Owner definition type
        type Owner {
            id: String!
            name: String!
            user: String!
            password: String!
            apiKey: String!
            enabled: Boolean!
        }

        input OwnerInput {
            name: String!
            user: String!
            password: String!
        }

    `;

    type(): string {
        return this.definitions
    }

}

const testDefinitions: string = new TestDefinition().type();

export default testDefinitions;