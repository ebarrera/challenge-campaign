export class TestOperations {
    
    private queries = `
        type Query {
            owners(first: Int, offset: Int): [Owner!]!
            owner(id: ID!): Owner
        }
    `;

    private mutations = `
        type Mutation {       
            createOwner(input: OwnerInput): Owner
            updateOwnerPassword(id:ID!, password: String!): Boolean
            deleteOwner(id: ID!): Boolean
        }
    `;

    operations(): string {
        return `
            ${this.queries}
            ${this.mutations}
        `
    }

}

const testOperations: string = new TestOperations().operations();

export default testOperations;