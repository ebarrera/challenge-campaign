import { GraphQLSchema } from 'graphql';
import { makeExecutableSchema } from 'graphql-tools';
import { merge } from 'lodash';

import { BaseSchema } from '../../BaseSchema';
import testDefinitions from '../otherImplementation/test.definitions';
import testOperations from '../otherImplementation/test.operations';
import { testResolvers } from '../otherImplementation/test.resolvers';

export class TestSchema extends BaseSchema {
    
    schema(): GraphQLSchema {
        const resolvers = merge( testResolvers );

        return makeExecutableSchema({
            typeDefs:[
                this.getDefinition(),
                testDefinitions,
                testOperations,
            ],
            resolvers
        });
    }

}

const testSchema: GraphQLSchema = new TestSchema().schema();

export default testSchema;