import { GraphQLResolveInfo } from 'graphql';
import { Transaction } from 'sequelize';

import { ResolverContext } from '../../../interfaces/ResolverContextInterface';
import { handleError, throwError } from '../../../utils/utils';
import { OwnerInstance } from '../../../models/OwnerModel';
import { compose } from '../../composable/composable.resolver';
import { authResolvers } from '../../composable/auth.resolver';

export const ownerResolvers = {
    
    Query: {
        owners: (parent, { first = 10, offset = 0}, context: ResolverContext, info: GraphQLResolveInfo ) => {
            return context.db.Owner
                        .findAll({
                            limit: first,
                            offset: offset,
                            attributes: context.requestedFields.getFields( info )
                        }).catch( handleError );
        },
        owner: (parent, {id}, context: ResolverContext, info: GraphQLResolveInfo ) => {
            return context.db.Owner.findById( id, {
                attributes: context.requestedFields.getFields( info )
            }).catch( handleError );
        },
        me: compose( ...authResolvers )((parent, args, context: ResolverContext, info: GraphQLResolveInfo ) => {
            return context.db.Owner.findById( context.authOwner.id, {
                attributes: context.requestedFields.getFields( info )
            }).catch( handleError );
        })
    },
    Mutation: {
        createOwner: (parent, {input}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.sequelize.transaction( (t: Transaction) => {
                return context.db.Owner.create( input, {transaction: t} );
            }).catch( handleError );
        },
        updateOwnerPassword: compose( ...authResolvers )((parent, {password}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.sequelize.transaction( (t: Transaction) => {
                return context.db.Owner
                            .findById( context.authOwner.id )
                            .then( (owner: OwnerInstance) => {
                                throwError(!owner, `User with id ${context.authOwner.id} not found` );
                                return owner.update( {password: password}, {
                                    where: {
                                        id: context.authOwner.id
                                    },
                                    transaction: t
                                }).then( owner => !!owner );
                            })
            })
        }),
        deleteOwner: compose( ...authResolvers )((parent, args, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.sequelize.transaction( (t: Transaction) => {
                return context.db.Owner
                            .findById( context.authOwner.id )
                            .then( (owner: OwnerInstance) => {
                                throwError(!owner, `User with id ${context.authOwner.id} not found` );
                                return owner.destroy({transaction: t})
                                                .then( owner => !!owner );
                            })
            })
        })
    }

}