const ownerTypes = `

    # Owner definition type
    type Owner {
        id: ID!
        name: String!
        user: String!
        enabled: Boolean!
    }

    input OwnerInput {
        name: String!
        user: String!
        password: String!
    }

`;

const ownerQueries = `
    owners(first: Int, offset: Int): [Owner!]!
    owner(id: ID!): Owner
    me: Owner
`;

const ownerMutations = `
    createOwner(input: OwnerInput): Owner
    updateOwnerPassword(password: String!): Boolean
    deleteOwner: Boolean
`;

export {
    ownerTypes,
    ownerQueries,
    ownerMutations
}