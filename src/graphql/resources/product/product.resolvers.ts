import { GraphQLResolveInfo } from 'graphql';
import { Transaction } from 'sequelize';

import { ResolverContext } from '../../../interfaces/ResolverContextInterface';
import { handleError, throwError } from '../../../utils/utils';
import { ProductInstance } from '../../../models/ProductModel';

export const productResolvers = {

    Query: {
        products: (parent, {first = 10, offset = 0}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.Product.findAll({
                limit: first,
                offset: offset,
                attributes: context.requestedFields.getFields( info )
            }).catch( handleError );
        },
        product: (parent, {id}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.Product.findById(id, {
                attributes: context.requestedFields.getFields( info )
            }).catch( handleError );
        }
    },
    Mutation: {
        createProduct: (parent, {input}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.sequelize.transaction( (t: Transaction) => {
                return context.db.Product.create( input, {transaction: t} )
            }).catch( handleError );
        },
        updateProduct: (parent, {id, input}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.sequelize.transaction( (t: Transaction) => {
                return context.db.Product.findById( id )
                        .then( (product: ProductInstance) => {
                            return product.update( input, {transaction: t} );
                        });
            }).catch( handleError );
        },
        deleteProduct: (parent, {id}, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.sequelize.transaction( (t: Transaction) => {
                return context.db.Product.findById( id )
                        .then( (product: ProductInstance) => {
                            return product.destroy( {transaction: t} )
                                        .then( promotion => !!promotion );
                        });
            }).catch( handleError );
        }
    }

}