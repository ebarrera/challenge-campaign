const productTypes = `
    
    # Product definition type 
    type Product {
        id: ID
        code: String!
        name: String!
        price: Float!
    }

    input ProductInput {
        code: String!
        name: String!
        price: Float!
    }

    input ProductRequestInput {
        id: ID!
        code: String!
        price: String
        name: String
    }

    input ProductUpdateInput {
        name: String
        price: Float
    }

`;

const productQueries = `
    products(first: Int, offset: Int): [Product!]!
    product(id: ID!): Product
`;

const productMutations = `
    createProduct(input: ProductInput!): Product
    updateProduct(id: ID!, input: ProductUpdateInput!): Product
    deleteProduct(id: ID!): Boolean
`;

export {
    productTypes,
    productQueries,
    productMutations
}