import { GraphQLResolveInfo } from 'graphql';
import { Transaction } from 'sequelize';

import { ResolverContext } from '../../../interfaces/ResolverContextInterface';
import { handleError, throwError } from '../../../utils/utils';
import { PromotionInstance } from '../../../models/PromotionModel';

export const promotionResolvers = {
    Promotion: {
        campaign: (promotion, args, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.dataloaders.campaignLoader
                    .load( {
                        key: promotion.get( 'campaign' ),
                        info
                    }).catch( handleError );
        }
    },
    Query: {
        promotions: (parent, {first = 10, offset = 0}, context: ResolverContext, info: GraphQLResolveInfo ) => {
            return context.db.Promotion
                        .findAll({
                            limit: first,
                            offset: offset,
                            attributes: context.requestedFields.getFields( info )
                        }).catch( handleError );
        },
        promotion: (parent, {id}, context: ResolverContext, info: GraphQLResolveInfo ) => {
            return context.db.Promotion.findById(id, {
                attributes: context.requestedFields.getFields( info )
            }).catch( handleError );
        }
    },
    Mutation: {
        createPromotion: (parent, {input}, context: ResolverContext, info: GraphQLResolveInfo ) => {
            return context.db.sequelize.transaction( (t: Transaction) => {
                return context.db.Promotion.create( input, {transaction: t} )
            }).catch( handleError );
        },
        updatePromotion: (parent, {id, input}, context: ResolverContext, info:GraphQLResolveInfo) => {
            return context.db.sequelize.transaction( (t: Transaction) => {
                return context.db.Promotion.findById( id )
                        .then( (promotion: PromotionInstance) => {
                            return promotion.update( input, {transaction: t} );
                        });
            }).catch( handleError );
        },
        deletePromotion: (parent, {id}, context: ResolverContext, info:GraphQLResolveInfo) => {
            return context.db.sequelize.transaction( (t: Transaction) => {
                return context.db.Promotion.findById( id )
                        .then( (promotion: PromotionInstance) => {
                            return promotion.destroy( {transaction: t} )
                                        .then( promotion => !!promotion );
                        });
            }).catch( handleError );
        }
    }
}