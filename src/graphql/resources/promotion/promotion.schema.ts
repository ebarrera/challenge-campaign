const promotionTypes = `
    
    # Promotion type definition
    type Promotion {
        id: ID!
        description: String!
        banner: String
        campaign: Campaign!
    }

    input PromotionInput {
        description: String!
        banner: String
        campaign: String!
    }

    input PromotionUpdateInput {
        description: String
        banner: String
    }
    
`;

const promotionQueries = `
    promotions(first: Int, offset: Int): [Promotion!]!
    promotion(id: ID!): Promotion
`;

const promotionMutations = `
    createPromotion( input: PromotionInput! ): Promotion
    updatePromotion( id: ID!, input: PromotionUpdateInput! ): Promotion
    deletePromotion( id: ID! ): Boolean
`;

export {
    promotionTypes,
    promotionQueries,
    promotionMutations
}