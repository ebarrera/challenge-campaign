import * as jwt from 'jsonwebtoken';

import { DbConnection } from "../../../interfaces/DbConnectionInterface";
import { OwnerInstance } from "../../../models/OwnerModel";
import { JWT_SECRET } from '../../../utils/utils';

export const tokenResolvers = {

    Mutation: {
        createToken: (parent, {input}, {db}: {db: DbConnection}) => {
            return db.Owner
                        .findOne({
                            where: {
                                user: input.user,
                                apiKey: input.apiKey
                            },
                            attributes: ['id', 'password', 'apiKey']
                        })
                        .then( (owner: OwnerInstance) => {
                            let errorMessage: string = 'Unauthorized, wrong email or password';
                            
                            if( !owner || !owner.isPassword(owner.get('password'), input.password) ) throw new Error(errorMessage);

                            // Info about payload https://jwt.io/introduction/ and https://tools.ietf.org/html/rfc7519#section-4.1
                            const payload = { 
                                sub: owner.get( 'id' ),
                                iss: owner.get( 'apiKey' )
                            };

                            return {
                                token: jwt.sign( payload, JWT_SECRET )
                            }
                        });
        }
    }

}