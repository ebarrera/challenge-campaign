const tokenTypes = `
    
    # Token definition type
    type Token {
        token: String!
    }

    input TokenInput {
        user: String!
        password: String!
        apiKey: String!
    }

`;

const tokenMutations = `
    createToken( input: TokenInput! ): Token
`;

export {
    tokenTypes,
    tokenMutations
}