import { makeExecutableSchema } from 'graphql-tools';
import { merge } from 'lodash';

import { Query } from './query';
import { Mutation } from './mutation';

import { campaignTypes } from './resources/campaign/campaign.schema';
import { ownerTypes } from './resources/owner/owner.schema';
import { promotionTypes } from './resources/promotion/promotion.schema';
import { conditionTypeTypes } from './resources/conditionType/conditionType.schema';
import { conditionTypes } from './resources/condition/condition.schema';
import { effectTypeTypes } from './resources/effectType/effectType.schema';
import { effectTypes } from './resources/effect/effect.schema';
import { tokenTypes } from './resources/token/token.schema';
import { customerTypes } from './resources/customer/customer.schema';
import { productTypes } from './resources/product/product.schema';
import { orderTypes } from './resources/order/order.schema';

import { campaignResolvers } from './resources/campaign/campaign.resolvers';
import { ownerResolvers } from './resources/owner/owner.resolvers';
import { promotionResolvers } from './resources/promotion/promotion.resolvers';
import { conditionTypeResolvers } from './resources/conditionType/conditionType.resolvers';
import { conditionResolvers } from './resources/condition/condition.resolvers';
import { effectTypeResolvers } from './resources/effectType/effectType.resolvers';
import { effectResolvers } from './resources/effect/effect.resolvers';
import { tokenResolvers } from './resources/token/token.resolvers';
import { productResolvers } from './resources/product/product.resolvers';
import { orderResolvers } from './resources/order/order.resolvers';

const resolvers = merge(
    tokenResolvers,
    campaignResolvers,
    ownerResolvers,
    promotionResolvers,
    conditionTypeResolvers,
    conditionResolvers,
    effectTypeResolvers,
    effectResolvers,
    productResolvers,
    orderResolvers
);

const SchemaDefinition = `
    type Schema {
        query: Query
        mutation: Mutation
    }
`;

export default makeExecutableSchema({
    typeDefs:[
        SchemaDefinition,
        Query,
        Mutation,
        tokenTypes,
        campaignTypes,
        ownerTypes,
        promotionTypes,
        conditionTypeTypes,
        conditionTypes,
        effectTypeTypes,
        effectTypes,
        customerTypes,
        productTypes,
        orderTypes
    ],
    resolvers
});