import * as http from 'http';

import app from './app';

import { normalizePort, onError, onListening } from './utils/utils';

const server = http.createServer( app );
const port : any = normalizePort( process.env.port || 3000 );

(async () => {
    server.listen( port );
    server.on( 'error', onError(server, port) );
    server.on( 'listening', onListening(server) );
})();