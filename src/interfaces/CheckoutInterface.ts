import { ProductInstance } from "../models/ProductModel";

export interface CustomerAttributes {
    name?: string;
    email?: string;
}

export interface OrderAttributes {
    customer?: CustomerAttributes;
    products?: ProductInstance[];
    subtotal?: number;
    discount?: number;
    total?: number;
}