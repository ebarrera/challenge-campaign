import { ConditionModel, ConditionInstance } from '../models/ConditionModel';
import { OrderAttributes } from './CheckoutInterface';

export interface ConditionValidator {
    validate(condition: ConditionInstance, order: OrderAttributes): boolean;
}