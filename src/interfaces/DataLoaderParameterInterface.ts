import { GraphQLResolveInfo } from 'graphql';

export interface DataLoaderParameter<T> {

    key: T;
    info: GraphQLResolveInfo;

}