import * as DataLoader from 'dataloader';

import { DataLoaderParameter } from './DataLoaderParameterInterface';

import { CampaignInstance } from '../models/CampaignModel';
import { PromotionInstance } from '../models/PromotionModel';
import { OwnerInstance } from '../models/OwnerModel';
import { ConditionTypeInstance } from '../models/ConditionTypeModel';
import { EffectTypeInstance } from '../models/EffectTypeModel';

export interface DataLoaders {
    
    campaignLoader: DataLoader<DataLoaderParameter<string>, CampaignInstance>;
    ownerLoader: DataLoader<DataLoaderParameter<string>, OwnerInstance>;
    promotionLoader: DataLoader<DataLoaderParameter<string>, PromotionInstance>;
    conditionTypeLoader: DataLoader<DataLoaderParameter<string>, ConditionTypeInstance>;
    effectTypeLoader: DataLoader<DataLoaderParameter<string>, EffectTypeInstance>;

}