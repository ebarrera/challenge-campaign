import { CampaignModel } from '../models/CampaignModel';
import { OwnerModel } from '../models/OwnerModel';
import { PromotionModel } from '../models/PromotionModel';
import { ConditionTypeModel } from '../models/ConditionTypeModel';
import { ConditionModel } from '../models/ConditionModel';
import { EffectTypeModel } from '../models/EffectTypeModel';
import { EffectModel } from '../models/EffectModel';
import { ProductModel } from '../models/ProductModel';

export interface ModelsInterface {

    Campaign: CampaignModel;
    Owner: OwnerModel;
    Promotion: PromotionModel;
    ConditionType: ConditionTypeModel;
    Condition: ConditionModel;
    EffectType: EffectTypeModel;
    Effect: EffectModel;
    Product: ProductModel

}