import { DbConnection } from './DbConnectionInterface';
import { DataLoaders } from './DataLoadersInterface';
import { RequestedFields } from '../graphql/ast/RequestedFields';
import { AuthOwner } from './AuthOwnerInterface';
import { ServiceLoaders } from './ServiceLoadersInterface';

export interface ResolverContext {

    db?: DbConnection;
    dataloaders?: DataLoaders;
    requestedFields?: RequestedFields;
    authorization?: string;
    authOwner?: AuthOwner;
    serviceloaders?: ServiceLoaders;

}