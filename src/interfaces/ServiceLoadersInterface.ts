import { CampaignService } from '../service/CampaignService';
import { PromotionService } from '../service/PromotionService';
import { ValidateService } from '../service/ValidateService';
import { ConditionService } from '../service/ConditionService';
import { ProductService } from '../service/ProductService';
import { EffectService } from '../service/EffectService';

export interface ServiceLoaders {

    campaignService: CampaignService;
    promotionService: PromotionService;
    validateService: ValidateService;
    conditionService: ConditionService;
    productService: ProductService;
    effectService: EffectService;

}