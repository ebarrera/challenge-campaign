import * as jwt from 'jsonwebtoken';
import { RequestHandler, Request, Response, NextFunction } from 'express';

import db from './../models';
import { JWT_SECRET } from '../utils/utils';
import { OwnerInstance } from '../models/OwnerModel';

export const extractJwtMiddleware = (): RequestHandler => {
    return (req: Request, res: Response, next: NextFunction ) :void => {

        let authorization : string = req.get( 'authorization' ); // Header Authorization - Bearer <token>
        let token: string = authorization ? authorization.split( ' ' )[1] : undefined;

        req['context'] = {};
        req['context']['authorization'] = authorization;

        if(!token) return next();

        jwt.verify( token, JWT_SECRET, (error, decoded: any) => {
            if( error ) return next();

            db.Owner.findOne({
                where: {
                    id: decoded.sub,
                    apiKey: decoded.iss
                },
                attributes: ['id', 'user']
            })
            .then( (owner: OwnerInstance) => {
                if( owner ){
                    req['context']['authOwner'] = {
                        id: owner.get( 'id' ),
                        user: owner.get( 'user' )
                    };
                }

                return next();
            });

        });

    };

}