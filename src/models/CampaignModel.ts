import * as Sequelize from 'sequelize';
import * as uuidv4 from 'uuid/v4';

import { BaseModelInterface } from '../interfaces/BaseModelInterface';
import { ModelsInterface } from '../interfaces/ModelsInterface';

export interface CampaignAttributes {
    id?: string;
    code?: string;
    name?: string;
    description?: string;
    isAutomatic?: boolean;
    enabled?: boolean;
    createdAt?: string;
    finalizedAt?: string;
    owner?: string;
}

export interface CampaignInstance extends Sequelize.Instance<CampaignAttributes>, CampaignAttributes {}

export interface CampaignModel extends BaseModelInterface, Sequelize.Model<CampaignInstance, CampaignAttributes> {}

export default (sequelize: Sequelize.Sequelize, DataTypes: Sequelize.DataTypes): CampaignModel => {
    
    const Campaign: CampaignModel = sequelize.define( 'Campaign', {
        id: {
            type: DataTypes.UUID,
            allowNull: false,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4
        },
        code: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        description: {
            type: DataTypes.STRING,
            allowNull: true
        },
        isAutomatic: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: true
        },
        enabled: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: true
        }
    }, {
        tableName: 'campaigns'
    });

    Campaign.associate = (models: ModelsInterface) => {
        Campaign.belongsTo( models.Owner, {
            foreignKey: {
                allowNull: false,
                field: 'owner',
                name: 'owner'
            }
        });
    }

    return Campaign;

}