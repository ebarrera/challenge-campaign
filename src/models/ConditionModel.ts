import * as Sequelize from 'sequelize';
import * as uuidv4 from 'uuid/v4';

import { BaseModelInterface } from '../interfaces/BaseModelInterface';
import { Comparator } from './enums/ComparatorEnum';
import { LogicalConnector } from './enums/LogicalConnectorEnum';
import { ModelsInterface } from '../interfaces/ModelsInterface';

export interface ConditionAttributes {
    id?: string;
    comparator?: string;
    value?: string;
    logicalConnector?: string;
    product?: string;
    promotion?: string;
    conditionType?: string;
}

export interface ConditionInstance extends Sequelize.Instance<ConditionAttributes>, ConditionAttributes {}

export interface ConditionModel extends BaseModelInterface, Sequelize.Model<ConditionInstance, ConditionAttributes> {}

export default (sequelize: Sequelize.Sequelize, DataTypes: Sequelize.DataTypes ): ConditionModel => {

    const Condition: ConditionModel = sequelize.define( 'Condition', {
        id: {
            type: DataTypes.UUID,
            allowNull: false,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4
        },
        comparator: {
            type: DataTypes.ENUM,
            values: Comparator.getAll(),
            allowNull: false
        },
        value: {
            type: DataTypes.STRING,
            allowNull: true
        },
        logicalConnector: {
            type: DataTypes.ENUM,
            values: LogicalConnector.getAll(),
            allowNull: false
        },
        product: {
            type: DataTypes.UUID,
            allowNull: true
        }
    }, {
        tableName: 'conditions'
    });

    Condition.associate = (models: ModelsInterface) => {
        Condition.belongsTo( models.Promotion, {
            foreignKey: {
                allowNull: false,
                field: 'promotion',
                name: 'promotion'
            }
        });

        Condition.belongsTo( models.ConditionType, {
            foreignKey: {
                allowNull: false,
                field: 'conditionType',
                name: 'conditionType'
            }
        });
    }

    return Condition;
}