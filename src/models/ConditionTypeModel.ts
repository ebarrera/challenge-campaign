import * as Sequelize from 'sequelize';
import * as uuidv4 from 'uuid/v4';

import { BaseModelInterface } from '../interfaces/BaseModelInterface';

export interface ConditionTypeAttributes {
    id?: string;
    code?: string;
    description?: string;
    enabled?: boolean;
}

export interface ConditionTypeInstance extends Sequelize.Instance<ConditionTypeAttributes>, ConditionTypeAttributes {}

export interface ConditionTypeModel extends BaseModelInterface, Sequelize.Model<ConditionTypeInstance, ConditionTypeAttributes> {}

export const ConditionTypes = {
    ITEM_ON_CART: 'ITEM_ON_CART',
    TO_PER_SOMETHING: 'TO_PER_SOMETHING'
}

export default (sequelize: Sequelize.Sequelize, DataTypes: Sequelize.DataTypes ): ConditionTypeModel => {

    const ConditionType: ConditionTypeModel = sequelize.define( 'ConditionType', {
        id: {
            type: DataTypes.UUID,
            allowNull: false,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4
        },
        code: {
            type: DataTypes.ENUM,
            values: Object.keys( ConditionTypes ),
            allowNull: false,
            unique: true
        },
        description: {
            type: DataTypes.STRING,
            allowNull: true
        },
        enabled: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: true
        }
    }, {
        tableName: 'condition_types'
    });

    return ConditionType;
}