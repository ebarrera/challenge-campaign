import * as Sequelize from 'sequelize';
import * as uuidv4 from 'uuid/v4';

import { BaseModelInterface } from '../interfaces/BaseModelInterface';
import { ModelsInterface } from '../interfaces/ModelsInterface';

export interface EffectAttributes {
    id?: string;
    value?: string;
    max?: number;
    product?: string;
    promotion?: string;
    effectType?: string;
}

export interface EffectInstance extends Sequelize.Instance<EffectAttributes>, EffectAttributes {}

export interface EffectModel extends BaseModelInterface, Sequelize.Model<EffectInstance, EffectAttributes> {}

export default (sequelize: Sequelize.Sequelize, DataTypes: Sequelize.DataTypes): EffectModel => {

    const Effect: EffectModel = sequelize.define( 'Effect', {
        id: {
            type: DataTypes.UUID,
            allowNull: false,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4
        },
        value: {
            type: DataTypes.STRING,
            allowNull: true
        },
        max: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: -1
        },
        product: {
            type: DataTypes.UUID,
            allowNull: true
        }
    }, {
        tableName: 'effects'
    });

    Effect.associate = (models: ModelsInterface) => {
        Effect.belongsTo( models.Promotion, {
            foreignKey: {
                allowNull: false,
                field: 'promotion',
                name: 'promotion'
            }
        });

        Effect.belongsTo( models.EffectType, {
            foreignKey: {
                allowNull: false,
                field: 'effectType',
                name: 'effectType'
            }
        });
    }

    return Effect;
}