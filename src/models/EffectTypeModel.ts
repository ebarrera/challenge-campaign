import * as Sequelize from 'sequelize';
import * as uuidv4 from 'uuid/v4';

import { BaseModelInterface } from '../interfaces/BaseModelInterface';

export interface EffectTypeAttributes {
    id?: string;
    code?: string;
    description?: string;
    enabled?: boolean;
}

export interface EffectTypeInstance extends Sequelize.Instance<EffectTypeAttributes>, EffectTypeAttributes {}

export interface EffectTypeModel extends BaseModelInterface, Sequelize.Model<EffectTypeInstance, EffectTypeAttributes> {}

export const EffectTypes = {
    ITEM_DISCOUNT_FIXED: 'ITEM_DISCOUNT_FIXED',
    ITEM_DISCOUNT_PERCENT: 'ITEM_DISCOUNT_PERCENT',
    CART_DISCOUNT_FIXED: 'CART_DISCOUNT_FIXED',
    CART_DISCOUNT_PERCENT: 'CART_DISCOUNT_PERCENT',
    TO_PER_SOMETHING: 'TO_PER_SOMETHING'
}

export default (sequelize: Sequelize.Sequelize, DataTypes: Sequelize.DataTypes): EffectTypeModel => {

    const EffectType: EffectTypeModel = sequelize.define( 'EffectType', {
        id: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4
        },
        code: {
            type: DataTypes.ENUM,
            values: Object.keys( EffectTypes ),
            allowNull: false,
            unique: true
        },
        description: {
            type: DataTypes.STRING,
            allowNull: true
        },
        enabled: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: true
        }
    }, {
        tableName: 'effect_types'
    });

    return EffectType;
}
