import * as Sequelize  from 'sequelize';
import { genSaltSync, hashSync, compareSync } from 'bcryptjs';

import { BaseModelInterface } from '../interfaces/BaseModelInterface';

export interface OwnerAttributes {
    id?: string,
    name?: string;
    user?: string;
    password?: string;
    apiKey?: string;
    enabled?: boolean;
}

export interface OwnerInstance extends Sequelize.Instance<OwnerAttributes>, OwnerAttributes {
    isPassword( encodedPassword: string, password: string ): boolean;
}

export interface OwnerModel extends BaseModelInterface, Sequelize.Model<OwnerInstance, OwnerAttributes> {}

export default (sequelize: Sequelize.Sequelize, DataTypes: Sequelize.DataTypes) : OwnerModel => {
    
    const Owner: OwnerModel = sequelize.define( 'Owner', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
            allowNull: false
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        user: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        apiKey: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            allowNull: false,
            unique: true
        },
        enabled: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        tableName: 'owners',
        hooks: {
            beforeCreate: (owner: OwnerInstance, options: Sequelize.CreateOptions) :void => {
                const salt = genSaltSync();
                owner.password = hashSync( owner.password, salt );
            },
            beforeUpdate: (owner: OwnerInstance, options: Sequelize.CreateOptions) :void => {
                if(owner.changed( 'password' )){
                    const salt = genSaltSync();
                    owner.password = hashSync( owner.password, salt );
                }
            }
        }
    });

    Owner.prototype.isPassword = (encodedPassword: string, password: string): boolean => {
        return compareSync( password, encodedPassword );
    }

    return Owner;
};