import * as Sequelize from 'sequelize';

import { BaseModelInterface } from '../interfaces/BaseModelInterface';

export interface ProductAttributes {
    id?: string;
    code?: string;
    name?: string;
    price?: number;
}

export interface ProductInstance extends Sequelize.Instance<ProductAttributes>, ProductAttributes {}

export interface ProductModel extends BaseModelInterface, Sequelize.Model<ProductInstance, ProductAttributes> {}

export default (sequelize: Sequelize.Sequelize, DataTypes: Sequelize.DataTypes): ProductModel => {

    const Product: ProductModel = sequelize.define( 'Product', {
        id: {
            type: DataTypes.UUID,
            allowNull: false,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4
        },
        code: {
            type: DataTypes.STRING,
            allowNull: false,
            set( val ) {
                this.setDataValue( 'code', val.toUpperCase() );
            }
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        price: {
            type: DataTypes.DECIMAL(15,2),
            allowNull: false
        }
    }, {
        tableName: 'products',
        indexes: [
            {
                fields: ['code', 'name'],
                unique: true
            }
        ]
    })

    return Product;
}