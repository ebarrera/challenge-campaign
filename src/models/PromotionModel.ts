import * as Sequelize from 'sequelize';

import { BaseModelInterface } from '../interfaces/BaseModelInterface';
import { ModelsInterface } from '../interfaces/ModelsInterface';

export interface PromotionAttributes {
    id?: string;
    description?: string;
    banner?: string;
    campaign?: string;
}

export interface PromotionInstance extends Sequelize.Instance<PromotionAttributes>, PromotionAttributes {}

export interface PromotionModel extends BaseModelInterface, Sequelize.Model<PromotionInstance, PromotionAttributes> {}

export default (sequelize: Sequelize.Sequelize, DataTypes: Sequelize.DataTypes): PromotionModel => {

    const Promotion: PromotionModel = sequelize.define( 'Promotion', {
        id: {
            type: DataTypes.UUID,
            allowNull: false,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4
        },
        description: {
            type: DataTypes.STRING,
            allowNull: false
        },
        banner: {
            type: DataTypes.TEXT,
            allowNull: true
        }
    },{
        tableName: 'promotions'
    });

    Promotion.associate = (models: ModelsInterface): void => {
        Promotion.belongsTo( models.Campaign, {
            foreignKey: {
                allowNull: false,
                field: 'campaign',
                name: 'campaign'
            }
        });
    }

    return Promotion;
}