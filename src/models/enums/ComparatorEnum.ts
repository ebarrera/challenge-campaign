import { merge } from  'lodash';

export const Comparators = {
    EQUAL_TO: 'EQUAL_TO',
    GREATHER_THAN: 'GREATHER_THAN',
    LESS_THAN: 'LESS_THAN',
    GREATHER_EQUAL_TO: 'GREATHER_EQUAL_TO',
    LESS_EQUAL_TO: 'LESS_EQUAL_TO',
    DISTINT: 'DISTINT'
}

export const ComparatorsBundle = {
    EQUAL_TO: {
        description: 'equal to',
        label: '=='
    },
    GREATHER_THAN: {
        description: 'greather than',
        label: '>'
    },
    LESS_THAN: {
        description: 'less than',
        label: '<'
    },
    GREATHER_EQUAL_TO: {
        description: 'greather equal to',
        label: '>='
    },
    LESS_EQUAL_TO: {
        description: 'less equal to',
        label: '<='
    },
    DISTINT: {
        description: 'distint',
        label: '!='
    }
}

export class Comparator {

    static getProperties(comparator): any {
        return ComparatorsBundle[comparator];
    }

    static getAll(): any {
        return merge( Object.keys(Comparators) );
    }

}