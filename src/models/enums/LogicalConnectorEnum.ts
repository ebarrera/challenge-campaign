import { merge } from  'lodash';

export const LogicalConnectors = {
    AND: '&&',
    OR: '||',
    NOT: '!'
}

export const LogicalConnectorsBundle = {
    AND: {
        description: 'and',
    }
}

export class LogicalConnector {

    static getProperties(comparator): any {
        return LogicalConnectorsBundle[comparator];
    }

    static getAll(): any {
        return merge( Object.keys(LogicalConnectors) );
    }

}