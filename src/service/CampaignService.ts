import { handleError } from '../utils/utils';
import { CampaignInstance, CampaignModel } from '../models/CampaignModel';

export class CampaignService {

    constructor( 
        private instance: CampaignModel
    ){}

    async getEnabledCampaigns(): Promise<string[]> {
        const campaigns: CampaignInstance[] = await this.instance.findAll({
            where: {
                enabled: true
            },
            attributes: ['id']
        }).catch( handleError );

        return campaigns.map( campaign => campaign.id );
    }

}