import { ConditionModel, ConditionInstance } from '../models/ConditionModel';
import { ConditionFactory } from './validators/ConditionFactory';
import { OrderAttributes } from '../interfaces/CheckoutInterface';
import { ConditionValidator } from '../interfaces/ConditionValidatorInterface';

export class ConditionService {

    constructor( 
        private instance: ConditionModel
    ){}

    async getConditionsByPromotion( promotionId ) {

        const conditions: ConditionInstance[] = await this.instance.findAll({
            where: {
                promotion: promotionId
            }
        });

        return conditions;
    }

    async couldApply( promotionId, order: OrderAttributes ) {
        const conditions: ConditionInstance[] = await this.getConditionsByPromotion( promotionId );
        let couldApply: boolean = false;

        for ( const condition of conditions ){
            couldApply = await this.validateCondition( condition, order);
        }

        if( conditions.length == 0 ) return true;

        return couldApply;
    }

    async validateCondition( condition: ConditionInstance, order: OrderAttributes ) {
        let validator: ConditionValidator = await ConditionFactory.getInstance( condition );
        if( validator ) return validator.validate(condition, order);
        return false;
    }

}