import { EffectFactory } from './appliers/EffectFactory';
import { OrderAttributes } from '../interfaces/CheckoutInterface';
import { EffectModel, EffectInstance } from '../models/EffectModel';
import { AbstractEffectApplier } from './appliers/AbstractEffectApplier';

export class EffectService {

    constructor( 
        private instance: EffectModel
    ){}

    async getEffectsByPromotion( promotionId ) {

        const effects: EffectInstance[] = await this.instance.findAll({
            where: {
                promotion: promotionId
            }
        });

        return effects;
    }

    async couldApply( promotionId, order: OrderAttributes ) {
        const effects: EffectInstance[] = await this.getEffectsByPromotion( promotionId );
        return effects.length > 0;
    }

    async applyEffects( promotionId, order: OrderAttributes): Promise<any> {
        const effects: EffectInstance[] = await this.getEffectsByPromotion( promotionId );
        const promotion = promotionId;

        let finalDiscount = 0;

        for( const effect of effects ){
            const discountApplied = await this.applyEffect( effect, order );
            finalDiscount += discountApplied;
        }

        return finalDiscount;
    }

    async applyEffect( effect: EffectInstance, order: OrderAttributes ): Promise<number> {
        let applier: AbstractEffectApplier = await EffectFactory.getInstance( effect );
        return applier.effect( effect ).order( order ).apply();
    }

}