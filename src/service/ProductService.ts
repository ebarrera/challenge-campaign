import { ProductModel, ProductInstance } from "../models/ProductModel";
import { throwError, handleError } from "../utils/utils";

export class ProductService {

    constructor(
        private instance: ProductModel
    ){}

    productById( id ){
        return this.instance.findById( id )
                    .then( (product: ProductInstance) => {
                        throwError( !product, `Product with id ${id} not found` );
                        return product;
                    }).catch( handleError );
    }

}