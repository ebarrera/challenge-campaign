import { handleError } from '../utils/utils';
import { PromotionModel, PromotionInstance } from '../models/PromotionModel';

export class PromotionService {

    constructor( 
        private instance: PromotionModel
    ){}

    async getPromotionsByCampaigns( ids: string[] ): Promise<string[]> {
        const promotions: PromotionInstance[] = await this.instance.findAll({
            where: {
                campaign: { $in: ids }
            },
            attributes: ['id']
        }).catch( handleError );

        return promotions.map( promotion => promotion.id );
    }


}