import { DbConnection } from '../interfaces/DbConnectionInterface';

import { ServiceLoaders } from '../interfaces/ServiceLoadersInterface';
import { CampaignService } from './CampaignService';
import { PromotionService } from './PromotionService';
import { ValidateService } from './ValidateService';
import { ConditionService } from './ConditionService';
import { ProductService } from './ProductService';
import { EffectService } from './EffectService';


export class ServiceLoaderFactory {

    constructor(
        private db: DbConnection
    ) {}

    getLoaders(): ServiceLoaders {
        const campaignService: CampaignService = new CampaignService(this.db.Campaign);
        const promotionService: PromotionService = new PromotionService(this.db.Promotion);
        const conditionService: ConditionService = new ConditionService(this.db.Condition);
        const productService: ProductService = new ProductService(this.db.Product);
        const effectService: EffectService = new EffectService(this.db.Effect);
        const validateService: ValidateService = new ValidateService();

        return {
            campaignService,
            promotionService,
            validateService,
            conditionService,
            productService,
            effectService
        }
    }

}