import serviceLoaders from './index';
import { OrderAttributes } from '../interfaces/CheckoutInterface';
import { handleError } from '../utils/utils';
import { ProductInstance } from '../models/ProductModel';

export class ValidateService {

    async validate( order: OrderAttributes ) {
        const campaignsId = await serviceLoaders.campaignService.getEnabledCampaigns();
        const promotionsId: string[] = await serviceLoaders.promotionService.getPromotionsByCampaigns(campaignsId);

        const promotionsDiscountMap = await this.validatePromotions( promotionsId, order );
        order = this.calculateDiscount( order, promotionsDiscountMap);
        order = this.calculateTotal( order );

        return order;
    }

    calculateDiscount( order: OrderAttributes, promotionsDiscountMap ) {
        let discount = 0;
        promotionsDiscountMap.map( (promoDiscount) => discount += promoDiscount.discountApplied);
        order.discount = discount;

        return order;
    }

    calculateTotal( order: OrderAttributes ){
        let total = 0;
        order.products.forEach( (p: ProductInstance) => total += +p.price);
        order.subtotal = total;
        order.total = total - order.discount;

        return order;
    }

    async validateProducts( order: OrderAttributes ){
        let itsOk = undefined;
        let transformFullFilled: ProductInstance[] = [];

        for( let product of order.products ){
            let found: ProductInstance = await serviceLoaders.productService.productById( product.id ).catch( handleError );

            if( !found ){
                return {
                    exist: true,
                    product: product.id,
                    message: `Product with ${product.id} not found`
                }
            }

            transformFullFilled.push( found );
            itsOk = undefined;
        }

        order.products = transformFullFilled

        return itsOk;
    }

    private async validatePromotions( promotionsId: string[], order: OrderAttributes ): Promise<any> {
        let discounts = [];

        for( const promotion of promotionsId ){
            const couldApply = await serviceLoaders.conditionService.couldApply( promotion, order );
            const hasAppliers = await serviceLoaders.effectService.couldApply( promotion, order );

            if( couldApply && hasAppliers ){
                console.log( `The promotion ${promotion} could apply`);
                const discountApplied = await serviceLoaders.effectService.applyEffects( promotion, order );                
                
                let promoDiscount = {
                    promotion,
                    discountApplied
                }

                discounts.push( promoDiscount );
            }else{
                console.log( `The promotion ${promotion} could not apply :(`);
            }
        }

        return discounts;
    }

}