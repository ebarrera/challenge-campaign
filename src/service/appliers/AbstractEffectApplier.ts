import { EffectInstance } from '../../models/EffectModel';
import { OrderAttributes } from '../../interfaces/CheckoutInterface';

export abstract class AbstractEffectApplier {

    protected _order: OrderAttributes = undefined;
    protected _effect: EffectInstance = undefined;

    effect( effect: EffectInstance ): AbstractEffectApplier {
        this._effect = effect;
        return this;
    }

    order( order: OrderAttributes ): AbstractEffectApplier {
        this._order = order;
        return this;
    }

    public abstract apply(): number;

}