import { AbstractEffectApplier } from './AbstractEffectApplier';

export class DefaultApplier extends AbstractEffectApplier {
    
    public apply(): number {
        return 0;
    }

}