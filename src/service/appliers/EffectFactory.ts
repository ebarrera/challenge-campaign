import db from '../../models';

import { EffectInstance } from '../../models/EffectModel';
import { EffectTypes } from '../../models/EffectTypeModel';
import { ItemDiscountFixed } from './ItemDiscountFixed';
import { AbstractEffectApplier } from './AbstractEffectApplier';
import { DefaultApplier } from './DefaultApplier';
import { ToPerSomethingApplier } from './ToPerSomethingApplier';

export class EffectFactory {

    static async getInstance(effect: EffectInstance): Promise<AbstractEffectApplier> {
        let instance = undefined;
        
        const effectType = await db.EffectType.findById( effect.effectType );
        const {code} = effectType;

        switch( code ){
            case EffectTypes.ITEM_DISCOUNT_FIXED:
                instance = new ItemDiscountFixed();
                break;
            case EffectTypes.TO_PER_SOMETHING:
                instance = new ToPerSomethingApplier();
                break;
            default:
                instance = new DefaultApplier();
                console.error( `Code EFFECT [${code}] has not implemented yet.` );
        }

        return <AbstractEffectApplier>instance;        
    }

}