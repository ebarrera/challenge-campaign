import { AbstractEffectApplier } from './AbstractEffectApplier';
import { ProductInstance } from '../../models/ProductModel';

export class ItemDiscountFixed extends AbstractEffectApplier {
    
    public apply(): number {
        console.log( `Applying effect ITEM DISCOUNT` );
        const value = +this._effect.value;
        const {product} = this._effect;
        const {products} = this._order;

        let discountApplied = 0;
        if( products.length == 0 ) return discountApplied;

        products.filter( (p: ProductInstance) => p.id == product)
            .forEach( (p: ProductInstance) => {
                discountApplied += (p.price - value);
            });

        return discountApplied;
    }


}