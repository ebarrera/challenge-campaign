import { AbstractEffectApplier } from './AbstractEffectApplier';
import { ProductInstance } from '../../models/ProductModel';

export class ToPerSomethingApplier extends AbstractEffectApplier {
    
    public apply(): number {
        console.log( `Applying effect TO PER SOMETHING` );
        if( !this._order.products || this._order.products.length == 0 ) return 0;

        const value: string[] = this._effect.value.split(',');
        const {product} = this._effect;
        const filterProduct = (p: ProductInstance) => p.id === product;

        const items: ProductInstance[] = this._order.products.filter( filterProduct );
        const unitPrice = items[0].price;
        const multipyFactor = +value[1];
        const unitaryFactor = items.length % +value[0]

        const factor = unitPrice * ( unitaryFactor + multipyFactor );
        const total = unitPrice * items.length;

        return total - factor;
    }

}