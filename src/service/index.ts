import db from '../models';
import { ServiceLoaderFactory } from './ServiceFactory';

const serviceLoadersInstance = new ServiceLoaderFactory( db );
const serviceLoaders = serviceLoadersInstance.getLoaders();

export default serviceLoaders;