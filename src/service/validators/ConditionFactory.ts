import db from '../../models';
import { ConditionInstance } from '../../models/ConditionModel';
import { ConditionTypes } from '../../models/ConditionTypeModel';
import { ItemOnCartValidator } from './ItemOnCartValidator';
import { ConditionValidator } from '../../interfaces/ConditionValidatorInterface';
import { DefaultValidator } from './DefaultValidator';
import { ToPerSomethingValidator } from './ToPerSomethingValidator';

export class ConditionFactory {

    static async getInstance(condition: ConditionInstance): Promise<ConditionValidator> {
        let instance = undefined;
        
        const conditionType = await db.ConditionType.findById( condition.conditionType );
        const {code} = conditionType;

        switch( code ){
            case ConditionTypes.ITEM_ON_CART:
                instance = new ItemOnCartValidator();
                break;
            case ConditionTypes.TO_PER_SOMETHING:
                instance = new ToPerSomethingValidator();
                break;
            default:
                instance = new DefaultValidator();
                console.error( `Code CONDITION [${code}] has not implemented yet.` );
        }

        return <ConditionValidator>instance;        
    }

}