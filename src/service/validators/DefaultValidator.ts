import { ConditionValidator } from '../../interfaces/ConditionValidatorInterface';
import { ConditionInstance } from '../../models/ConditionModel';
import { OrderAttributes } from '../../interfaces/CheckoutInterface';

export class DefaultValidator implements ConditionValidator {
    validate( condition: ConditionInstance, order: OrderAttributes ): boolean {
        return false;
    }
}