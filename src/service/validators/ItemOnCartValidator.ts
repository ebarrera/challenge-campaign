import { ConditionValidator } from '../../interfaces/ConditionValidatorInterface';
import { OrderAttributes } from '../../interfaces/CheckoutInterface';
import { ConditionInstance } from '../../models/ConditionModel';
import { Comparators } from '../../models/enums/ComparatorEnum';
import { ProductAttributes } from '../../models/ProductModel';

export class ItemOnCartValidator implements ConditionValidator {

    validate(condition: ConditionInstance, order: OrderAttributes): boolean {
        if( !order.products || order.products.length == 0 ) return false;

        const value: number = +condition.value;
        const {product} = condition;
        const {comparator} = condition;
        const items: number = order.products.filter( (p: ProductAttributes) => p.id === product ).length;

        let evualuate: boolean = false;

        switch( comparator ){
            case Comparators.GREATHER_EQUAL_TO:
                evualuate = (items >= value);
                break;
        }
        
        return evualuate;
    }

}