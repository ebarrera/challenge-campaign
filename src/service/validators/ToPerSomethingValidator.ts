import { ConditionValidator } from '../../interfaces/ConditionValidatorInterface';
import { OrderAttributes } from '../../interfaces/CheckoutInterface';
import { ConditionInstance } from '../../models/ConditionModel';
import { Comparators } from '../../models/enums/ComparatorEnum';
import { ProductInstance } from '../../models/ProductModel';

export class ToPerSomethingValidator implements ConditionValidator {

    validate(condition: ConditionInstance, order: OrderAttributes): boolean {
        if( !order.products || order.products.length == 0 ) return false;

        const value: string[] = condition.value.split(',');
        const required: number = +value[0];
        const {product} = condition;
        const {comparator} = condition;
        const items: number = order.products.filter( (p: ProductInstance) => p.id === product ).length;

        let evualuate: boolean = false;

        switch( comparator ){
            case Comparators.EQUAL_TO:
                evualuate = Math.floor(items / required) > 0;
                break;
        }
        
        return evualuate;
    }

}