import { errorType } from './constants';

export const getErrorCode = errorName => errorType[errorName];