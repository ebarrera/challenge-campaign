import * as jwt from 'jsonwebtoken';

import { db, chai, handleError, expect } from '../../test-utils';
import app from '../../../src/app';
import { OwnerInstance } from '../../../src/models/OwnerModel';
import { JWT_SECRET } from '../../../src/utils/utils';

describe( 'Owner', () => {

    let token: string;
    let ownerId: string;

    beforeEach( () => {
        return db.Condition.destroy({where: {}})
                .then((rows: number) => db.Effect.destroy({where: {}}))
                .then((rows: number) => db.ConditionType.destroy({where: {}}))        
                .then((rows: number) => db.EffectType.destroy({where: {}}))
                .then((rows: number) => db.Promotion.destroy({where: {}}))
                .then((rows: number) => db.Campaign.destroy({where: {}}))
                .then((rows: number) => db.Owner.destroy({where: {}}))
                .then((rows: number) => db.Owner.create(
                        {
                            name: 'Rocket',
                            user: 'rocket.raccoon@marvel.com',
                            password: '1234',
                            apiKey: '7aae731f-4ca0-4b55-a18a-7f95708bd2c5'
                        }
                ))
                .then( (owner: OwnerInstance) => {
                    ownerId = owner.get( 'id' );

                    const payload = {
                        sub: ownerId,
                        iss: owner.get( 'apiKey' )
                    };

                    token = jwt.sign( payload, JWT_SECRET );
                })
                .catch( handleError );
    });

    describe( 'Queries', () => {
        describe( 'application/json', () => {

            describe( 'owners', () => {

                it( 'should return a list of owners', () => {
                    let body = {
                        query: `
                            query {
                                owners {
                                    name
                                    user
                                }
                            }
                        `
                    };

                    return chai.request(app)
                                .post( '/graphql' )
                                .set( 'content-type', 'application/json' )
                                .send( JSON.stringify(body) )
                                .then( res => {
                                    const ownersList = res.body.data.owners;
                                    expect( res.body.data ).to.be.an( 'object' );
                                    expect( ownersList ).to.be.an( 'array' ).with.length.greaterThan(0);
                                    expect( ownersList[0] ).to.have.keys( ['user', 'name'] )
                                }).catch( handleError );
                });
                
            });

            describe( 'me', () => {

                it( 'should return my own information', () => {
                    let body = {
                        query: `
                            query {
                                me {
                                    id
                                    name
                                    user
                                }
                            }
                        `
                    };

                    return chai.request(app)
                                .post( '/graphql' )
                                .set( 'content-type', 'application/json' )
                                .set( 'authorization', `Bearer ${token}`)
                                .send( JSON.stringify(body) )
                                .then( res => {
                                    const me = res.body.data.me;
                                    expect( res.body.data ).to.be.an( 'object' );
                                    expect( me ).to.be.an( 'object' ).to.have.keys( ['id', 'user', 'name'] );
                                    expect( me.id ).to.equal( ownerId );
                                }).catch( handleError );
                });

            });

        })
    })

    describe( 'Mutations', () => {

        describe( 'createOwner', () => {
            it( 'should create new owner', () => {

                let body = {
                    query: `
                        mutation createNewOwner($input: OwnerInput!){
                            createOwner(input: $input){
                                id
                                user
                                name
                            }
                        }
                    `,
                    variables: {
                        input: {
                            name: 'Drax',
                            user: 'drax@marvel.com',
                            password: '1234'
                        }
                    }
                };

                return chai.request(app)
                            .post( '/graphql')
                            .set( 'content-type', 'application/json')
                            .send(JSON.stringify(body))
                            .then( res => {
                                const createdOwner = res.body.data.createOwner;
                                expect( createdOwner ).to.be.an( 'object' );
                                expect( createdOwner.name ).to.equal( 'Drax' );
                                expect( createdOwner.id ).to.be.a( 'string' ); 
                            }).catch( handleError );
            });
        });

        describe( 'updateOwnerPassword', () => {
            it( 'should update password on existing owner', () => {
                let body = {
                    query: `
                        mutation updateOwnerPassword($password: String!){
                            updateOwnerPassword(password: $password)
                        }
                    `,
                    variables: {
                        password: 'rocket1234'
                    }
                };

                return chai.request(app)
                            .post( '/graphql')
                            .set( 'content-type', 'application/json')
                            .set( 'authorization', `Bearer ${token}` )
                            .send(JSON.stringify(body))
                            .then( res => {
                                expect( res.body.data.updateOwnerPassword ).to.be.true;
                            }).catch( handleError );
            });
        });

        describe( 'deleteOwner', () => {
            it( 'should my profile', () => {
                let body = {
                    query: `
                        mutation {
                            deleteOwner
                        }
                    `
                };

                return chai.request(app)
                            .post( '/graphql')
                            .set( 'content-type', 'application/json')
                            .set( 'authorization', `Bearer ${token}` )
                            .send(JSON.stringify(body))
                            .then( res => {
                                expect( res.body.data.deleteOwner ).to.be.true;
                            }).catch( handleError );
            });
        });

    });

})