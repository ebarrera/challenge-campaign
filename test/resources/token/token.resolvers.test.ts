import { db, chai, handleError, expect } from '../../test-utils';
import app from '../../../src/app';

describe( 'Token', () => {

    beforeEach( () => {
        return db.Condition.destroy({where: {}})
                .then((rows: number) => db.Effect.destroy({where: {}}))
                .then((rows: number) => db.ConditionType.destroy({where: {}}))        
                .then((rows: number) => db.EffectType.destroy({where: {}}))
                .then((rows: number) => db.Promotion.destroy({where: {}}))
                .then((rows: number) => db.Campaign.destroy({where: {}}))
                .then((rows: number) => db.Owner.destroy({where: {
                    user: 'rocket.raccoon@marvel.com',
                    apiKey: '7aae731f-4ca0-4b55-a18a-7f95708bd2c5'
                }}))
                .then((rows: number) => db.Owner.create(
                        {
                            name: 'Rocket',
                            user: 'rocket.raccoon@marvel.com',
                            password: '1234',
                            apiKey: '7aae731f-4ca0-4b55-a18a-7f95708bd2c5'
                        }
                )).catch( handleError );
    });

    describe( 'Mutations', () => {
        describe( 'application/json', () => {

            describe( 'createToken', () => {

                it( 'should return a new valid token', () => {
                    let body = {
                        query: `
                            mutation createToken($input: TokenInput!){
                                createToken(input: $input){
                                    token
                                }
                            }
                        `,
                        variables: {
                            input: {
                                user: 'rocket.raccoon@marvel.com',
                                password: '1234',
                                apiKey: '7aae731f-4ca0-4b55-a18a-7f95708bd2c5'
                            }
                        }
                    }

                    return chai.request(app)
                                .post( '/graphql' )
                                .set( 'content-type', 'application/json' )
                                .send(JSON.stringify(body))
                                .then( res => {
                                    expect( res.body.data ).to.have.key( 'createToken' );
                                    expect( res.body.data.createToken ).to.have.key( 'token' );
                                    expect( res.body.data.createToken.token ).to.be.string;
                                    expect( res.body.errors ).to.be.undefined;
                                }).catch( handleError );

                });
                
                it( 'should return an error if the password, user or apiKey are incorrect', () => {
                    let body = {
                        query: `
                            mutation createToken($input: TokenInput!){
                                createToken(input: $input){
                                    token
                                }
                            }
                        `,
                        variables: {
                            input: {
                                user: 'rocket.raccoon@marvel.com',
                                password: 'ilovedc',
                                apiKey: '7aae731f-4ca0-4b55-a18a-7f95708bd2c5'
                            }
                        }
                    }

                    return chai.request(app)
                                .post( '/graphql' )
                                .set( 'content-type', 'application/json' )
                                .send(JSON.stringify(body))
                                .then( res => {
                                    expect( res.body ).to.have.keys( ['data', 'errors'] );
                                    expect( res.body.data ).to.have.key( 'createToken' );
                                    expect( res.body.data.createToken ).to.be.null;
                                    expect( res.body.errors ).to.be.an('array').with.length(1);
                                    expect( res.body.errors[0].message ).to.equal( 'Unauthorized, wrong email or password' );
                                }).catch( handleError );
                });

            });

        })
    })

})